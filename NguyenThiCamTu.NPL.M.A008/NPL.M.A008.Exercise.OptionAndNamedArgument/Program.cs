﻿
using NPL.M.A008.Exercise;
using System.Diagnostics;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Xml.Linq;

Console.OutputEncoding = Encoding.Unicode;

//Perform Student object initialization using Optional Arguments
Student student1 = new Student("A", "KS05", "Nữ", 20);
Student student2 = new Student("B", "KS05", "Nam", 21, "Single");
Student student3 = new Student("C", "KS05", "Nữ", 22, "Maried", 10);
Student student4 = new Student("D", "KS05", "Nam", 19, "Single", 9, "A");

//Perform Student object initialization using Named Arguments
Student student5 = new Student(Name: "E", Class: "KS05", Gender: "Nữ", Age: 27);
Student student6 = new Student(Age: 21, Name: "F", Gender: "Nam", Class: "KS05", Relationship: "Single");
Student student7 = new Student("G", "KS05", "Nữ", Age: 24, Relationship: "Maried", 10);
Student student8 = new Student("H", Class: "KS05", "Nam", 28, "Single", 9, "A");

//Implement the Graduate method call with transmission parameter:
Console.WriteLine("Call Grade Method with transmission parameter: ");
Console.WriteLine(student1.Graduate(4.0M));
Console.WriteLine(student5.Graduate(2.1M));
Console.WriteLine();

////Implement the Graduate method call with no parameter:
Console.WriteLine("Call Grade Method with no parameter: ");
Console.WriteLine(student2.Graduate());
Console.WriteLine(student6.Graduate());
Console.WriteLine();

//Implement ToString method with no parameter:
Console.WriteLine("Call ToString Method with no parameter: ");
Console.WriteLine();
Console.WriteLine(String.Format("{0,-5} {1,-10} {2,-10} {3,-15} {4, -5} {5,-10}\n",
                                 "Name", "Class", "Gender", "Relationship", "Age", "Grade"));
Console.WriteLine(student1.ToString());
Console.WriteLine(student2.ToString());
Console.WriteLine(student3.ToString());
Console.WriteLine(student4.ToString());
Console.WriteLine(student5.ToString());
Console.WriteLine(student6.ToString());
Console.WriteLine(student7.ToString());
Console.WriteLine(student8.ToString());
Console.WriteLine();

//Implement ToString method with tranmission parameter:
Console.WriteLine("Call ToString Method with tranmission parameter: ");
Console.WriteLine();
Console.WriteLine(String.Format("{0,-5} {1,-10} {2,-10} {3,-15} {4, -5} {5,-10}\n",
                                 "Name", "Class", "Gender", "Relationship", "Age", "Grade"));
Console.WriteLine(student1.ToString(student1.Name, student1.Class, student1.Gender, student1.Relationship, student1.Age, student1.Grade));
Console.WriteLine(student2.ToString(student2.Name, student2.Class, student2.Gender, student2.Relationship, student2.Age, student2.Grade));
Console.WriteLine(student3.ToString(student3.Name, student3.Class, student3.Gender, student3.Relationship, student3.Age, student3.Grade));
Console.WriteLine(student4.ToString(student4.Name, student4.Class, student4.Gender, student4.Relationship, student4.Age, student4.Grade));
Console.WriteLine(student5.ToString(student5.Name, student5.Class, student5.Gender, student5.Relationship, student5.Age, student5.Grade));
Console.WriteLine(student6.ToString(student6.Name, student6.Class, student6.Gender, student6.Relationship, student6.Age, student6.Grade));
Console.WriteLine(student7.ToString(student7.Name, student7.Class, student7.Gender, student7.Relationship, student7.Age, student7.Grade));
Console.WriteLine(student8.ToString("Bonus", "KS04", "Nam", "Single", 30, "B"));
Console.WriteLine();
Console.ReadKey();


// The benefits of using Optional Arguments:
// Gán giá trị mặc định cho 1 tham số khi không được truyền giá trị vào tham số đó
// Ví dụ: student1 không gán giá trị cho grade nên student1 có grade = 'F' (F là giá trị mặc định)

// Tiện hơn trong việc gán giá trị cho các tham số

// The benefits of using Named Arguments:
// Không cần nhớ thứ tự các tham số trong constrcutor vẫn có thể khởi tạo được
//Ví dụ: Student student6 = new Student(Age: 21 ,Name:"F",Gender: "Nam", Class:"KS05",Relationship: "Single");
//       dù constructor là  Student (name, class, gender, age, relationship, mark, grade)
// Cách khai báo rõ ràng hơn

// The rules when using Optional Arguments
// Các đối số tự chọn có gắn giá trị mặc định ở sau cùng các dối số bắt buộc
// Ví dụ: public Student (string Name, string Class, string Gender, int Age, string Relationship = "Single", decimal Mark = 0, string Grade = "F")

// the rules when using Named Arguments:
// Khi trộn lẫn các đối số dùng Named Arguments và không dùng Named Arguments thì bắt buộc phải đúng thứ tự:
// Ví dụ đúng: Student student7 = new Student("G", "KS05", "Nữ", Age:24, Relationship: "Maried", 10);
// Ví dụ sai: Student student7 = new Student("KS05", "H", "Nữ", Relationship: "Maried", 10, Age:24);
