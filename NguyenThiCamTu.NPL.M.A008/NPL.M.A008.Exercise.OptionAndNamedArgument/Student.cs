﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A008.Exercise
{
    internal class Student
    {
        public string Name { get; set; }
        public string Class { get; set; }
        public string Gender { get; set; }
        public string Relationship { get; set; }
        public DateOnly EntryDate { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public decimal Mark { get; set; }
        public string Grade { get; set; }

        public Student()
        { }
        public Student(string Name, string Class, string Gender, int Age, string Relationship = "Single", decimal Mark = 0, string Grade = "F")
        {
            this.Name = Name;
            this.Class = Class;
            this.Gender = Gender;
            this.Age = Age;
            this.Relationship = Relationship;
            this.Mark = Mark;
            this.Grade = Grade;
        }
        public string Graduate(decimal gradePoint = 0)
        {
            if (gradePoint == 0M)
            {
                Grade = "F";
            }
            else if (gradePoint == 1.0M)
            {
                Grade = "D";
            }
            else if (gradePoint == 2.0M)
            {
                Grade = "C";
            }
            else if (gradePoint == 2.3M)
            {
                Grade = "C+";
            }
            else if (gradePoint == 2.7M)
            {

                Grade = "B-";
            }
            else if (gradePoint == 3.0M)
            {

                Grade = "B";
            }
            else if (gradePoint == 3.3M)
            {
                Grade = "B+";
            }
            else if (gradePoint == 3.7M)
            {
                Grade = "A-";
            }
            else if (gradePoint == 4.0M)
            {
                Grade = "A+";
            }
            return Grade;
        }

        public string ToString(string Name, string Class, string Gender, string Relationship, int Age, string Grade)
        {
            return String.Format("{0,-5} {1,-10} {2,-10} {3,-15} {4, -5} {5,-10}\n",
                                 Name, Class, Gender, Relationship, Age, Grade);
        }
    }

}
