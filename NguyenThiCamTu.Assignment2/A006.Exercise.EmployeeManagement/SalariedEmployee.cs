﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A006.Exercise.EmployeeManagement
{
    internal class SalariedEmployee : Employee
    {
        private double commissionRate { get; set; }
        private double grossSales { get; set; }
        private double basicSalary { get; set; }

        public SalariedEmployee() {
        }

        public SalariedEmployee(
            string ssn,
            string firstName,
            string lastName,
            DateTime birthDate,
            string phone,
            string email,
            double commissionRate,
            double grossSales,
            double basicSalary) : base(ssn, firstName, lastName, birthDate, phone, email)
        {
            this.commissionRate = commissionRate;
            this.grossSales = grossSales;
            this.basicSalary = basicSalary;
        }

        public override string? ToString()
        {
            return base.ToString() + String.Format("{0, -12} {1, -12} {2, -12} ", commissionRate, grossSales, basicSalary);
        }



    }

}
