﻿using System.Text.RegularExpressions;

class SortedListByName
{
    //Hamf GetLastName trả về danh sách LastName của danh sách FullName nhập vào
    public static string[] GetLastName(string[] names)
    {
        string[] lastNames = new string[names.Length];
        for (int i = 0; i < names.Length; i++)
        {
            string[] nameElement = names[i].Split(" ");
            lastNames[i] = nameElement[nameElement.Length - 1];
        }
        return lastNames;
    }

    //Hàm SortName để sắp xếp tên theo bảng chữ cái của last name
    public static string[] SortName(string[] names)
    {
        string[] lastNames = GetLastName(names);
        //Vòng lặp for để sắp xếp đồng thời last name và full name
        for (int i = 0; i < lastNames.Length - 1; i++)
        {
            for (int j = 0; j < lastNames.Length - i - 1; j++)
            {
                if (string.Compare(lastNames[j], lastNames[j + 1]) > 0)
                {
                    string lastNameTemp = lastNames[j];
                    lastNames[j] = lastNames[j + 1];
                    lastNames[j + 1] = lastNameTemp;

                    string nameTemp = names[j];
                    names[j] = names[j + 1];
                    names[j + 1] = nameTemp;
                }
            }
        }
        return names;
    }

    public static void Main(string[] args)
    {
        Console.WriteLine("Enter list of full name (seperate by ,): ");
        string list = Console.ReadLine();
        String pattern = @"^[a-zA-Z, ]+$";
        while (!Regex.IsMatch(list, pattern))
        {
            Console.WriteLine("Invalid Format! Enter again: ");
            list = Console.ReadLine();
        }
        string[] names = list.Split(",", StringSplitOptions.RemoveEmptyEntries);
        Console.WriteLine("List of full name before sorted by alphabet: ");
        for (int i = 0; i < names.Length; i++)
        {
            
            if (i == names.Length - 1)
            {
                Console.WriteLine(names[i]);
            }
            else
                Console.Write(names[i] + ",");
        }    
        names = SortName(names);
        Console.WriteLine();
        Console.WriteLine("List of full name after sorted by alphabet: ");
        for (int i = 0; i < names.Length; i++)
        {
          
            if (i == names.Length - 1)
            {
                Console.WriteLine(names[i]);
            }
            else
                Console.Write(names[i] + ",");
        }
        Console.ReadKey();
    }
}





//private static String InputStringOfRegexWithMessage(String message, String regex)
//{
//    while (true)
//    {
//        Console.Write(message);
//        String res = Console.ReadLine();
//        if (Regex.IsMatch(res, regex))
//        {
//            Console.WriteLine("\nAccepted input:\n" + res);
//            return res;
//        }
//        else
//        {
//            Console.Write("Wrong format! Enter again: ");
//        }
//    }
//}
//private static void PrintStringArr(String[] strArr)
//{
//    Console.WriteLine("String after sorting:");
//    for (int i = 0; i < strArr.Length; i++)
//    {
//        Console.Write(strArr[i]);
//        if (i < strArr.Length - 1)
//            Console.Write(", ");
//    }
//    Console.ReadLine();
//}
//}
