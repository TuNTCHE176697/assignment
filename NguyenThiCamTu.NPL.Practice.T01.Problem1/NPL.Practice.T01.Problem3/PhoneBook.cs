﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T01.Problem3
{
    internal class PhoneBook
    {
        public string name { get; set; }
        public string phoneNumBer { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public Group group { get; set; }

        public PhoneBook() { }
        public PhoneBook(string name, string phoneNumber, string email, string address, Group group)
        {
            this.name = name;
            this.phoneNumBer = phoneNumber;
            this.email = email;
            this.address = address;
            this.group = group;

        }
        public void ToString()
        {
            Console.WriteLine("{0,-15} {1,-17} {2,-15} {3,-20} {4,-10}", name, phoneNumBer, email, address, group);
        }
    }
}
