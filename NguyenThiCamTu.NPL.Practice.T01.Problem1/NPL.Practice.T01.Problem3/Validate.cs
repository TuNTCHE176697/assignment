﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NPL.Practice.T01.Problem3
{
    internal class Validate
    {
        //Hàm ValidatePhoneNumber để ép người dùng nhập số điện thoại đúng forat
        public static string ValidatePhoneNumber()
        {
            string pattern = @"^0\d{2}\s\d{3}\s\d{4}$";

            while (true)
            {
                string name = Console.ReadLine();
                if (Regex.IsMatch(name, pattern))
                {
                    return name;
                }
                else
                {
                    throw new FormatException("Input value is not correct!");

                }
            }

        }

        public static string ValidateGroup()
        {

            while (true)
            {

                string group = Console.ReadLine();
                if (!group.Equals("Family") && !group.Equals("Colleague") && !group.Equals("Friend") && !group.Equals("Other"))
                {
                    throw new FormatException("Input value is not correct!");
                }
                else
                    return group;

            }
        }
        //Hàm ValidateContinueChoice ép người dùng nhập vào string a hay b
        public static string ValidateContinueChoice(string a, string b)
        {
            while (true)
            {
                string choice = Console.ReadLine().ToString();
                if (choice == a || choice == b || choice == a.ToLower() || choice == b.ToLower())
                {
                    return choice;
                }
                else
                {
                    Console.WriteLine("Invalid Choice!Please enter again");
                    Console.Write("Enter your option (" + a + " or " + b + "): ");
                }
            }
        }


        //Hàm ValidateChoice để ép người dùng nhập vào số nguyên trong khoảng yêu cầu
        public static int ValidateChoice(int min, int max)
        {
            while (true)
            {
                string StringInput = Console.ReadLine();
                try
                {
                    if (string.IsNullOrEmpty(StringInput))
                    {
                        Console.Write("Chưa nhập lựa chọn!\nNhập lại số: ");
                    }
                    else
                    {
                        int IntegerInput = int.Parse(StringInput);
                        if (IntegerInput < min || IntegerInput > max)
                        {
                            Console.WriteLine("Out of range! Enter number from " + min + " to " + max);
                            Console.Write("Enter a number from " + min + " to " + max + ": ");
                        }
                        else return IntegerInput;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid number! Enter number from " + min + " to " + max);
                    Console.Write("Enter a number from " + min + " to " + max + ": ");
                }
            }
        }



    }
}
