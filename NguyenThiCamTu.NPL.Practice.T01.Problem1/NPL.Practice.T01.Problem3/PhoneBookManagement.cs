﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace NPL.Practice.T01.Problem3
{
    internal class PhoneBookManagement
    {
        List<PhoneBook> phoneBooks = new List<PhoneBook>();

        public void Add()
        {

            PhoneBook phoneBook = new PhoneBook();
            Console.Write("Enter Name: ");
            phoneBook.name = Console.ReadLine();
            Console.Write("Enter Phone Number: ");

            string a = null;
            bool b = true;
            while (b)
            {
                a = Validate.ValidatePhoneNumber();
                var result = from pb in phoneBooks
                             where pb.phoneNumBer == a
                             select pb;
                if (result.Count() > 0)
                {
                    Console.WriteLine("Duplicate Phone Number!");
                    Console.Write("Enter Phone Number again: ");
                }
                else
                {
                    b = false;
                }
            }

            phoneBook.phoneNumBer = a;
            Console.Write("Enter Email: ");
            phoneBook.email = Console.ReadLine();
            Console.Write("Enter address: ");
            phoneBook.address = Console.ReadLine();
            Console.Write("Enter group: ");
            string group = Validate.ValidateGroup();
            if (group == "Family")
            {
                phoneBook.group = Group.Family;
            }
            if (group == "Colleague")
            {
                phoneBook.group = Group.Colleague;
            }
            if (group == "Friend")
            {
                phoneBook.group = Group.Friend;
            }
            if (group == "Other")
            {
                phoneBook.group = Group.Other;
            }

            phoneBooks.Add(phoneBook);

            Console.Write("Do you want to continue add Phone Book Information? (Yes/No): ");
            string choice = Validate.ValidateContinueChoice("Yes", "No");
            if (choice.Equals("yes"))
            {
                Add();
            }
        }
        public void Show()
        {
            Console.WriteLine("{0,-15} {1,-17} {2,-15} {3,-20} {4,-10}", "Name", "Phone Number", "Email", "Address", "Group");
            foreach (PhoneBook phoneBook in phoneBooks)
            {
                phoneBook.ToString();
            }
        }
        public void Remove()
        {
            Console.WriteLine("Remove Phone Book By Name ");
            Console.Write("Enter Phone Book Name: ");
            string name = Console.ReadLine();
            var result = (from pb in phoneBooks
                         where pb.name.ToLower() == name.ToLower()
                         select pb).ToList();
            if (result.Count() > 0)
            {
                foreach (PhoneBook pb in result)
                {
                    phoneBooks.Remove(pb);
                }
                Console.WriteLine("Phone Book has Name = " + name + " is successfully remove!");
                Console.WriteLine("Phone Book List after removing: ");
                Show();
            }
            else
            {
                Console.WriteLine("Phone Book has Name = " + name + " is not exist!");
                Console.WriteLine("Phone Book List is not changed!");
            }

        }
        public void Sort()
        {
            var result = phoneBooks.OrderBy(x => x.name);
            Console.WriteLine("Phone Book List after Sorting by Name: ");
            Console.WriteLine("{0,-15} {1,-17} {2,-15} {3,-20} {4,-10}", "Name", "Phone Number", "Email", "Address", "Group");
            foreach (var pb in result)
            {
                pb.ToString();
            }
        }

        public void Find()
        {
            Console.Write("Find Phone Book By Name ");
            Console.Write("Enter Phone Book Name: ");
            string name = Console.ReadLine();
            var result = from pb in phoneBooks
                         where pb.name.ToLower().Contains(name.ToLower())
                         select pb;
            if (result.Count() > 0)
            {
                Console.WriteLine("Phone Book has Name = " + name + " Information: ");
                Console.WriteLine("{0,-15} {1,-17} {2,-15} {3,-20} {4,-10}", "Name", "Phone Number", "Email", "Address", "Group");
                foreach (var pb in result)
                {
                    pb.ToString();
                }
            }
            else
            {
                Console.WriteLine("Phone Book has Name = " + name + " is not exist!");

            }
        }
    }
}
