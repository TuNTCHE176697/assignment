﻿using NPL.Practice.T01.Problem3;

Console.InputEncoding = System.Text.Encoding.Unicode;
Console.OutputEncoding = System.Text.Encoding.Unicode;

PhoneBookManagement management = new PhoneBookManagement();
while (true)
{
    Console.WriteLine("Phone Book Management Program");
    Console.WriteLine("1. Add a new Phone Book");
    Console.WriteLine("2. Remove Phone Book by Name");
    Console.WriteLine("3. Sort Phone Book by Name");
    Console.WriteLine("4. Find Phone Book by Name");
    Console.WriteLine("0. Exit");

    Console.Write("Enter menu option: ");
    int choice = Validate.ValidateChoice(0, 4);

    switch (choice)
    {
        case 1:
            management.Add();
            break;

        case 2:
            management.Remove();
            break;

        case 3:
            management.Sort();
            break;

        case 4:
            management.Find();
            break;
        case 0:
            // Thoát khỏi ứng dụng
            Environment.Exit(0);
            break;

        default:
            Console.WriteLine("Invalid option!");
            break;
    }
}
