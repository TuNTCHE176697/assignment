﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T01.Problem01
{
    internal class Validate
    {
        //Hàm ValidateNumber dùng để ép người dùng nhập vào số nguyên dương
        public static int ValidateNumber(string character)
        {
            while (true)
            {
                string StringInput = Console.ReadLine();
                try
                {
                    if (string.IsNullOrEmpty(StringInput))
                    {
                        Console.Write("The " + character + " must not be empty!\nEnter a number again: ");
                    }
                    else
                    {
                        int intInput = int.Parse(StringInput);
                        if (intInput <= 0)
                        {
                            Console.WriteLine("Invalid number! The " + character + " must be positive!");
                            Console.Write("Enter a number again: ");
                        }
                        else return intInput;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid number! The " + character + " must be positive integer");
                    Console.Write("Enter a number again: ");
                }
            }
        }

    }
}
