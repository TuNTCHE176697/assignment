﻿using System;
namespace NPL.Practice.T01.Problem01
{
    class Program
    {
        //Hàm DrawCircleChart để biến dãy số nguyên dương thành số decimal với tổng các số decimal là 100
        private static decimal[] DrawCircleChart(int[] charInput)
        {
            //Tính tổng tất cả số nguyên dương trong mảng charInput
            int sum = 0;
            for(int i = 0;  i < charInput.Length; i++)
            {
                sum += charInput[i];
            }

            //Vòng lặp tiếp theo chia từng phần tử trong mảng charInput cho tổng sum nhân 100 và làm tròn 2 chữ số sau thập phân và bỏ vào mảng decimal[] result
            decimal[] result = new decimal[charInput.Length];

            for(int i = 0;i < result.Length;i++)
            {
                result[i] = ((decimal)charInput[i] / sum) * 100;
                result[i] = Math.Round(result[i], 2);
            }
          
            return result;
        }

        public static void Main(string[] args) {
            //Nhập vào kích thước mảng
            Console.Write("Enter size of positive integers array: ");
            int n = Validate.ValidateNumber("size");
            int[] intergerInput = new int[n];

            //Nhập phần tử trong mảng
            Console.WriteLine("Enter each element in positive integers array: ");
            for (int i = 0; i<intergerInput.Length; i++)
            {
                Console.Write("Enter element ["+(i+1)+"]: ");
                intergerInput[i] = Validate.ValidateNumber("postive integer element");
            }

            //In ra mảng đầu vào
            Console.Write("Positive integers arrays before convert: ");
            Console.Write("{");
            for (int i = 0; i < intergerInput.Length; i++)
            {

                if (i == intergerInput.Length - 1)
                {
                    Console.WriteLine(intergerInput[i] + "}");
                }
                else
                Console.Write(intergerInput[i] + ", ");

            }


            //Gọi hàm DrawCircleChart
            decimal[] result = DrawCircleChart(intergerInput);

            //In ra kết quả
            Console.Write("Decimal arrays after convert: ");
            Console.Write("{");
            for (int i = 0; i < result.Length; i++)
            {

                if (i == result.Length - 1)
                {
                    Console.Write(result[i] + "}");
                }
                else
                Console.Write(result[i]+"; ");

            }

        }
    }
}