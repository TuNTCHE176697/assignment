﻿using NPL.Practice.T02.Problem2;

namespace NPL.Practice.T02
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.InputEncoding = System.Text.Encoding.Unicode;
            Console.OutputEncoding = System.Text.Encoding.Unicode;


            //Điền vào danh sách họ và tên, từng tên cách nhau bởi dấu ,
            Console.Write("Enter List of Full Name (Separate by , ): ");
            string fullName = Validate.ValidateName();

            //Tách từng họ và tên và bỏ vào mảng fullNameSplit
            string[] fullNameSplit = fullName.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            List<string> hoTen = fullNameSplit.ToList();
            List<string> emails = GenerateEmailAddress(hoTen);

            Console.WriteLine("List of emails after convert: ");
            foreach (string email in emails)
            {
                Console.WriteLine(email);
            }

        }
        static private List<String> GenerateEmailAddress(List<String> emailAddresses)
        {
            List<string> emails = new List<string>();

            List<string> nameBeforeEmail = new List<string>();

            //Thực hiện thao tác từng tên trong mảng emailAddresses
            foreach (string b in emailAddresses)
            {
                string a = Validate.RemoveDiacritics(b);
                string result = String.Empty;
                //Tìm vị trí dấu cách cuối cùng
                int index = a.LastIndexOf(" ");
      
                if (index != -1)
                {
                    //Lấy lastname của tên
                    result += a.Substring(index).ToLower();

                    //Tách thành firtname, middle name, last name
                    string[] element = a.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    //Lấy từng chữ cái đầu của firstname và lastname
                    foreach (string element2 in element)
                    {
                        if (element2.Equals(element[element.Length - 1]))
                            continue;

                        //Nối từng chữ cái đầu vào last name
                        result += element2.Substring(0, 1).ToLower();

                    }
                }
                else
                    result += a.ToLower();

                //Xem result tồn tại bao nhiêu lần trong danh sách
                int count = 0;
                foreach (string element3 in nameBeforeEmail)
                {
                    if (element3.Contains(result))
                    {
                        count++;
                    }
                }
                //Nếu trùng thì tăng đuôi lên 1
                if (count <= 0)
                    {
                        //Thêm từng phần tử vào list nameBeforeEmail
                        nameBeforeEmail.Add(result);
                        //Thêm đuôi @fsoft.com.vn rồi thêm vào list emails
                        result += "@fsoft.com.vn";
                        emails.Add(result);
                    }
                else
                    {
                        result += (count);
                        nameBeforeEmail.Add(result);
                        //Thêm đuôi @fsoft.com.vn rồi thêm vào list emails
                        result += "@fsoft.com.vn";
                        emails.Add(result);
                    }

              
            }
            return emails;
        }
    }
}