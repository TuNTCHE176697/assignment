﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem2
{
    internal class Validate
    {
        //Hàm ValidateName ép người dùng nhập vào chữ cái
        public static string ValidateName()
        {
            string pattern = @"[A-Za-zÁ-ỹ\s,]+";
            while (true)
            {
                string name = Console.ReadLine();
                if (Regex.IsMatch(name, pattern))
                {
                    return name;
                }
                else
                {
                    Console.WriteLine("Invalid List of Full Name! Please Enter only characters and separator ,!");
                    Console.Write("Enter again: ");
                }
            }
        }

        public static string RemoveDiacritics(string input)
        {
            string normalizedString = input.Normalize(NormalizationForm.FormD);
            StringBuilder stringBuilder = new StringBuilder();

            foreach (char c in normalizedString)
            {
                if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                    stringBuilder.Append(c);
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

    }
}
