﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentRegistrationManagement
{
    internal class RegistrationManagement
    {
        List<Student> StudentList = new List<Student>();
        //{
        //    new Student { studentID = 1, studentName = "Nguyễn Văn A", studentAge = 20, studentDepartment = "Công nghệ thông tin" },
        //    new Student { studentID = 2, studentName = "Trần Thị B", studentAge = 22, studentDepartment = "Toán học" },
        //    new Student { studentID = 3, studentName = "Lê Văn C", studentAge = 21, studentDepartment = "Công nghệ thông tin" },
        //    new Student { studentID = 4, studentName = "Phạm Văn D", studentAge = 23, studentDepartment = "Vật lý" },
        //    new Student { studentID = 5, studentName = "Hoàng Thị E", studentAge = 24, studentDepartment = "Toán học" }
        //};

        List<Subject> subjectList = new List<Subject>();
        //{
        //    new Subject { SubjectID = 101, SubjectName = "Lập trình cơ bản" },
        //    new Subject { SubjectID = 102, SubjectName = "Toán cao cấp" },
        //    new Subject { SubjectID = 103, SubjectName = "Vật lý cơ bản" }
        //};

        List<Registration> registrationList = new List<Registration>();
        //{
        //    new Registration { studentID = 1, subjectID = 101, mark = 10 },
        //    new Registration { studentID = 1, subjectID = 102, mark = 9 },
        //    new Registration { studentID = 2, subjectID = 101, mark = 8 },
        //    new Registration { studentID = 3, subjectID = 101, mark = 7 },
        //    new Registration { studentID = 4, subjectID = 102, mark = 6 },
        //    //new Registration { studentID = 5, subjectID = 103, mark = 5 }
        //};


        public void AddStudent()
        {
            Console.WriteLine("Enter Student Information:");
            Student student = new Student();
            int stID = 0;
            var countID = 0;
            do
            {
                Console.Write("Enter Student ID: ");
                stID = Validate.ValidateNumber("Student ID");
                countID = StudentList.Where(x => x.studentID == stID).Count();
            } while (countID > 0);

            student.studentID = stID;
            Console.Write("Enter Student Name: ");
            student.studentName = Validate.ValidateName();
            Console.Write("Enter Student Age: ");
            student.studentAge = Validate.ValidateNumber("Student Age");
            Console.Write("Enter Student Department: ");
            student.studentDepartment = Validate.ValidateName();
            StudentList.Add(student);

            Console.Write("Do you want to continue?(Yes/No): ");
            string option = Validate.ValidateContinueChoice("Yes", "No");
            if (option.ToLower().Equals("yes"))
            {
                AddStudent();
            }
        }


        public void AddSubject()
        {
            Console.WriteLine("Enter Subject Information:");
            Subject subject = new Subject();
            int sjID = 0;
            var countID = 0;
            do
            {
                Console.Write("Enter Subject ID: ");
                sjID = Validate.ValidateNumber("Subject ID");
                countID = subjectList.Where(x => x.SubjectID == sjID).Count();
            } while (countID > 0);
            subject.SubjectID = sjID;

            string sjName;
            var countName = 0;
            do
            {
                Console.Write("Enter Subject Name: ");
                sjName = Validate.ValidateName();
                countName = subjectList.Where(x => x.SubjectName.Equals(sjName)).Count();
            } while (countName > 0);
            subject.SubjectName = sjName;

            subjectList.Add(subject);

            Console.Write("Do you want to continue?(Yes/No): ");
            string option = Validate.ValidateContinueChoice("Yes", "No");
            if (option.ToLower().Equals("yes"))
            {
                AddSubject();
            }
        }

        public void AddRegistration()
        {
            L1: Console.WriteLine("Enter Registration Information:");
            Registration registration = new Registration();
            int checkSt = 0;
            int stID = 0;
            int checkSj = 0;
            int sjID = 0;

            do
            {
                Console.Write("Enter Student ID: ");
                stID = Validate.ValidateNumber("Student ID");
                checkSt = StudentList.Where(x => x.studentID == stID).Count();
            } while (checkSt == 0);

            do
            {
                Console.Write("Enter Subject ID: ");
                sjID = Validate.ValidateNumber("Subject ID");
                checkSj = subjectList.Where(x => x.SubjectID == sjID).Count();
            } while (checkSj == 0);

            int check = (from rg in registrationList
                         where (rg.studentID == stID) && rg.studentID == sjID
                         select rg).Count();

            if (check == 1)
            {
                Console.WriteLine("Student already registered!");
                goto L1;
            }

            registration.studentID = stID;
            registration.subjectID = sjID;
            Console.WriteLine("Enter Student Mark: ");
            registration.mark = Validate.ValidateMark(0, 10);

            registrationList.Add(registration);

            Console.Write("Do you want to continue?(Yes/No): ");
            string option = Validate.ValidateContinueChoice("Yes", "No");
            if (option.ToLower().Equals("yes"))
            {
                AddRegistration();
            }
        }

        public void StudentWithNumberSubject()
        {
            var studentWithSubjectList = from sv in StudentList
                                         join dk in registrationList on sv.studentID equals dk.studentID into result
                                         select new
                                         {
                                             svID = sv.studentID,
                                             svName = sv.studentName,
                                             numSubject = result.Count(),
                                         };
            int numberRegistedStudent = (from re in registrationList
                                         join sv in StudentList on re.studentID equals sv.studentID 
                                         group re by re.studentID into result
                                         select result).Count();


            Console.WriteLine("Number of students have registed subject: "+numberRegistedStudent+"/"+StudentList.Count());
            Console.WriteLine("Number of students have not registed subject: " + (subjectList.Count()-numberRegistedStudent) + "/" + StudentList.Count());
            Console.WriteLine("{0,-15} {1,-20} {2,-30}","Student ID", "Student Name", "Number Of Registered Subject");
            foreach (var sv in studentWithSubjectList)
            {
                Console.WriteLine("{0,-15} {1,-20} {2,-30}",sv.svID,sv.svName, sv.numSubject);
            }
        }

        public void SubjectWithNumberStudent()
        {
            var SubjectWithStudentList = from sj in subjectList
                                         join dk in registrationList on sj.SubjectID equals dk.subjectID into result
                                         select new
                                         {
                                             sjID = sj.SubjectID,
                                             sjName = sj.SubjectName,
                                             numStudent = result.Count(),
                                         };
            int numberRegistedSubject = (from re in registrationList
                                         join sj in subjectList on re.subjectID equals sj.SubjectID
                                         group re by re.subjectID into result
                                         select result).Count();


            Console.WriteLine("Number of subject have registered students: " + numberRegistedSubject + "/" + subjectList.Count());
            Console.WriteLine("Number of students have no registered student: " + (StudentList.Count() - numberRegistedSubject) + "/" + subjectList.Count());
            Console.WriteLine("{0,-15} {1,-20} {2,-30}", "Subject ID", "Subject Name", "Number Of Registered Student");
            foreach (var sv in SubjectWithStudentList)
            {
                Console.WriteLine("{0,-15} {1,-20} {2,-30}", sv.sjID, sv.sjName, sv.numStudent);
            }
        }

        public void NumberOfStudent()
        {
            var students = from sv in StudentList
                           select sv;
            Console.WriteLine("Number of students: " + students.Count());
            Console.WriteLine("{0,-15} {1,-20} {2,-15} {3,-30}", "Student ID", "Student Name", "Student Age", "Student Department");
            foreach (var student in students)
            {
                Console.WriteLine("{0,-15} {1,-20} {2,-15} {3,-30}", student.studentID, student.studentName, student.studentAge, student.studentDepartment);
            }

        }
        public void SubjectHaveStudents()
        {
            var SubjectStudent= from sj in subjectList
                                join dk in registrationList on sj.SubjectID equals dk.subjectID 
                                group dk by new { dk.subjectID, sj.SubjectName } into result
                                select new
                                {
                                    sjID = result.Key.subjectID,
                                    sjName = result.Key.SubjectName,
                                    numStudent = result.Count(),
                                };

            Console.WriteLine("Number of subjects have students: " +SubjectStudent.Count()+"/"+subjectList.Count());
            Console.WriteLine("{0,-15} {1,-20} {2,-30}", "Subject ID", "Subject Name", "Number Of Registered Student");
            foreach (var sv in SubjectStudent)
            {
                Console.WriteLine("{0,-15} {1,-20} {2,-30}", sv.sjID, sv.sjName, sv.numStudent);
            }
        }

        public void SubjectHaveNoStudents()
        {           
            var SubjectStudent = from sj in subjectList
                                 where !(from dk in registrationList select dk.subjectID).Contains(sj.SubjectID)
                                 select new
                                 {
                                     sjID = sj.SubjectID,
                                     sjName = sj.SubjectName,
                                     numStudent = 0,
                                 };
            Console.WriteLine("Number of subjects have students: " + SubjectStudent.Count() + "/" + subjectList.Count());
            Console.WriteLine("{0,-15} {1,-20} {2,-30}", "Subject ID", "Subject Name", "Number Of Registered Student");
            foreach (var sv in SubjectStudent)
            {
                Console.WriteLine("{0,-15} {1,-20} {2,-30}", sv.sjID, sv.sjName, sv.numStudent);
            }
        }

        public void StudentWithAverageMark()
        {
            Console.WriteLine("Number of students: " + StudentList.Count());
            Console.WriteLine("{0,-15} {1,-20} {2,-15} {3,-30} {4,-20}", "Student ID", "Student Name", "Student Age", "Student Department","Average Mark");
            foreach (Student sv in StudentList)
            {
                var AverageMark = (from rg in registrationList
                                   where rg.studentID == sv.studentID
                                   select rg.mark).DefaultIfEmpty(0).Average();

                Console.WriteLine("{0,-15} {1,-20} {2,-15} {3,-30} {4,-20}", sv.studentID, sv.studentName, sv.studentAge, sv.studentDepartment,AverageMark);
            }

        }
    }
}

