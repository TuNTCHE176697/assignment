﻿using StudentRegistrationManagement;
Console.InputEncoding = System.Text.Encoding.Unicode;
Console.OutputEncoding = System.Text.Encoding.Unicode;

RegistrationManagement management = new RegistrationManagement();
while (true)
{
    Console.WriteLine("Student Registration Management Program");
    Console.WriteLine("1. Add a new student");
    Console.WriteLine("2. Add a new subject");
    Console.WriteLine("3. Add registration and mark of a student");
    Console.WriteLine("4. Display list student and number of registered subjects");
    Console.WriteLine("5. Display list subject and number of students registered");
    Console.WriteLine("6. Display number of students");
    Console.WriteLine("7. Display number of subject have students");
    Console.WriteLine("8. Display number of subject have no student");
    Console.WriteLine("9. Display list student and average mark");
    Console.WriteLine("0. Exit");

    Console.Write("Enter menu option: ");
    int choice = Validate.ValidateChoice(0, 9);

    switch (choice)
    {
        case 1:
            management.AddStudent();
            break;

        case 2:
            management.AddSubject();
            break;

        case 3:
            management.AddRegistration();
            break;

        case 4:
            management.StudentWithNumberSubject();
            break;

        case 5:
            management.SubjectWithNumberStudent();
            break;

        case 6:
            management.NumberOfStudent();
            break;

        case 7:
            management.SubjectHaveStudents();
            break;

        case 8:
            management.SubjectHaveNoStudents();
            break;

        case 9:
            management.StudentWithAverageMark();
            break;

        case 0:
            // Thoát khỏi ứng dụng
            Environment.Exit(0);
            break;

        default:
            Console.WriteLine("Invalid option!");
            break;
    }
}