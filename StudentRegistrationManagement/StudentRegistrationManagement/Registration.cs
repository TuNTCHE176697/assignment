﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentRegistrationManagement
{
    internal class Registration
    {
        public int studentID { get; set; }
        public int subjectID { get; set; }
        public float mark { get; set; }
    }
}
