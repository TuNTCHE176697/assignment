﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentRegistrationManagement
{
    internal class Student
    {
        public int studentID { get; set; }
        public string studentName { get; set; }
        public int studentAge { get; set; }
        public string studentDepartment { get; set; }
    }
}
