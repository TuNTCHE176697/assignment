﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StudentRegistrationManagement
{
    internal class Validate
    {
        //Hàm ValidateNumber dùng để ép người dùng nhập vào số nguyên lớn hơn 0
        public static int ValidateNumber(string character)
        {
            while (true)
            {
                string StringInput = Console.ReadLine();
                try
                {
                    if (string.IsNullOrEmpty(StringInput))
                    {
                        Console.Write("The " + character + " must not be empty!\nEnter a number again: ");
                    }
                    else
                    {
                        int IntInput = int.Parse(StringInput);
                        if (IntInput <= 0)
                        {
                            Console.WriteLine("Invalid number! The " + character + " must be greater than 0!");
                            Console.Write("Enter a number again: ");
                        }
                        else return IntInput;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid number! The " + character + " must be greater than 0!");
                    Console.Write("Enter a number again: ");
                }
            }
        }
        //Hàm ValidateContinueChoice ép người dùng nhập vào string a hay b
        public static string ValidateContinueChoice(string a, string b)
        {
            while (true)
            {
                string choice = Console.ReadLine().ToString();
                if (choice == a || choice == b || choice == a.ToLower() || choice == b.ToLower())
                {
                    return choice;
                }
                else
                {
                    Console.WriteLine("Invalid Choice!Please enter again");
                    Console.Write("Enter your option (" + a + " or " + b + "): ");
                }
            }
        }


        //Hàm ValidateName dùng để ép người dùng nhập vào chữ cái 
        public static string ValidateName()
        {
            string pattern = @"^[A-Za-zÁ-ỹ\s]+$";
            while (true)
            {
                string name = Console.ReadLine().ToString();
                if (Regex.IsMatch(name, pattern))
                {
                    return name;
                }
                else
                {
                    Console.WriteLine("Invalid Name! Please Enter only characters!");
                    Console.Write("Enter again: ");
                }
            }
        }


        //Hàm ValidateChoice để ép người dùng nhập vào số nguyên trong khoảng yêu cầu
        public static int ValidateChoice(int min, int max)
        {
            while (true)
            {
                string StringInput = Console.ReadLine();
                try
                {
                    if (string.IsNullOrEmpty(StringInput))
                    {
                        Console.Write("Chưa nhập lựa chọn!\nNhập lại số: ");
                    }
                    else
                    {
                        int IntegerInput = int.Parse(StringInput);
                        if (IntegerInput < min || IntegerInput > max)
                        {
                            Console.WriteLine("Out of range! Enter number from " + min + " to " + max);
                            Console.Write("Enter a number from " + min + " to " + max + ": ");
                        }
                        else return IntegerInput;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid number! Enter number from " + min + " to " + max);
                    Console.Write("Enter a number from " + min + " to " + max + ": ");
                }
            }
        }
        //Hàm ValidateMark để ép người dùng nhập vào số float trong khoảng yêu cầu
        public static float ValidateMark(float min, float max)
        {
            while (true)
            {
                string StringInput = Console.ReadLine();
                try
                {
                    if (string.IsNullOrEmpty(StringInput))
                    {
                        Console.Write("Chưa nhập lựa chọn!\nNhập lại số: ");
                    }
                    else
                    {
                        float FloatInput = float.Parse(StringInput);
                        if (FloatInput < min || FloatInput > max)
                        {
                            Console.WriteLine("Out of range! Enter number from " + min + " to " + max);
                            Console.Write("Enter a number from " + min + " to " + max + ": ");
                        }
                        else return FloatInput;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid input! Enter number from " + min + " to " + max);
                    Console.Write("Enter a number from " + min + " to " + max + ": ");
                }
            }
        }

    }
}
