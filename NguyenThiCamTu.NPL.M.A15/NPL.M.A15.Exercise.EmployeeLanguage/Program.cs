﻿using NguyenThiCamTu.NPL.M.A015;
using System.ComponentModel;
using System.Text;
Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;

EmployeeLanguageManagement management = new EmployeeLanguageManagement();
management.Initial();

Console.WriteLine("========= Assignment 15 - Employee Programming Language Management=========");
int choice;

do
{
    Console.WriteLine("1. Enter New Employee Information");
    Console.WriteLine("2. Enter New Programing Language Information");
    Console.WriteLine("3. Enter New Department Information");
    Console.WriteLine("4. Enter New Employee And Programming Languages Information");
    Console.WriteLine("5. Get Departments with Minimum Employees");
    Console.WriteLine("6. Get Working Employees");
    Console.WriteLine("7. Get Employees by Programming Language");
    Console.WriteLine("8. Get Programming Languages by Employee ID");
    Console.WriteLine("9. Get Senior Employees with Multiple Programming Language");
    Console.WriteLine("10. Get Employees with Paging and Sorting");
    Console.WriteLine("11. Get All Departments with Belonged Employees");
    Console.WriteLine("0. Exit");
    Console.Write("Enter Menu Option Number: ");
    choice = Validate.ValidateChoice(0, 11);

    switch (choice)
    {
        case 1:
            management.AddEmployee();
            break;

        case 2:
            management.AddProgramLanguage();
            break;
        case 3:
            management.AddDepartment();
            break;
        case 4:
            management.AddSkill();
            break;
        case 5:
            management.GetDepartMentWithMinimumEmployees();
            break;
        case 6:
            management.GetEmployeesWorking();
            break;
        case 7:
            management.GetEmployeeByLanguage();
            break;
        case 8:
            management.GetLanguageWithEmployeeID();
            break;
        case 9:
            management.GetSeniorEmployee();
            break;
        case 10:
            management.GetEmployeByPaging();
            break;
        case 11:
            management.GetDepartments();
            break;
        case 0:
            Console.WriteLine("Exiting the program.");
            Environment.Exit(0);
            break;
        default:
            Console.WriteLine("Invalid choice. Please enter a number between 1 and 8.");
            break;
    }
}
while (choice > 0 && choice <= 11);
Console.ReadKey();

