﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NguyenThiCamTu.NPL.M.A015
{
    internal class Validate
    {
        //Hàm ValidateChoice dùng để ép người dùng nhập vào lựa chọn là kiểu số nguyên trong phạm vi yêu cầu
        public static int ValidateChoice(int min, int max)
        {
            while (true)
            {
                string StringInput = Console.ReadLine();
                try
                {
                    if (string.IsNullOrEmpty(StringInput))
                    {
                        Console.Write("The option must not be empty!\nEnter a number again: ");
                    }
                    else
                    {
                        int IntegerInput = int.Parse(StringInput);
                        if (IntegerInput < min || IntegerInput > max)
                        {
                            Console.WriteLine("Invalid number! Please enter an number from " + min + " to " + max);
                            Console.Write("Enter a number from " + min + " to " + max + ": ");
                        }
                        else return IntegerInput;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid number! Please enter an number from " + min + " to " + max);
                    Console.Write("Enter a number from " + min + " to " + max + ": ");
                }
            }
        }
        //Hàm ValidateNumber dùng để ép người dùng nhập vào số nguyên lớn hơn 0
        public static int ValidateNumber(string character)
        {
            while (true)
            {
                string StringInput = Console.ReadLine();
                try
                {
                    if (string.IsNullOrEmpty(StringInput))
                    {
                        Console.Write("The " + character + " must not be empty!\nEnter a number again: ");
                    }
                    else
                    {
                        int IntInput = int.Parse(StringInput);
                        if (IntInput <= 0)
                        {
                            Console.WriteLine("Invalid number! The " + character + " must be greater than 0!");
                            Console.Write("Enter a number again: ");
                        }
                        else return IntInput;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid number! The " + character + " must be greater than 0!");
                    Console.Write("Enter a number again: ");
                }
            }
        }

        //Hàm ValidateContinueChoice ép người dùng nhập vào string a hay b
        public static string ValidateContinueChoice(string a, string b)
        {
            while (true)
            {
                string choice = Console.ReadLine().ToString();
                if (choice == a || choice == b || choice == a.ToLower() || choice == b.ToLower())
                {
                    return choice;
                }
                else
                {
                    Console.WriteLine("Invalid Choice!Please enter again");
                    Console.Write("Enter your option (" + a + " or " + b + "): ");
                }
            }
        }
        //Hàm ValidateName dùng để ép người dùng nhập vào chữ cái 
        public static string ValidateName()
        {
            string pattern = @"^[A-Za-zÁ-ỹ\s]+$";
            while (true)
            {
                string name = Console.ReadLine().ToString();
                if (Regex.IsMatch(name, pattern))
                {
                    return name;
                }
                else
                {
                    Console.WriteLine("Invalid Name! Please Enter only characters!");
                    Console.Write("Enter again: ");
                }
            }
        }
        //Hàm ValidateDate dùng để ép người dùng nhập vào ngày sinh có dạng dd/MM/yyyy
        public static DateOnly ValidateDate(string character)
        {
            while (true)
            {
                try
                {
                    string input = Console.ReadLine();
                    DateOnly inputDate = DateOnly.ParseExact(input, "dd/MM/yyyy", null);
                    return inputDate;
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid Datetime! Please enter " + character + " format (dd/MM/yyyy)! ");
                    Console.Write("Enter " + character + " format (dd/MM/yyyy): ");
                }

            }

        }

    }
}
