﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NguyenThiCamTu.NPL.M.A015
{

    internal class EmployeeLanguageManagement
    {
        public List<Employee> employees = new List<Employee>();
        public List<Department> departments = new List<Department>();
        public List<ProgrammingLanguage> programmingLanguages = new List<ProgrammingLanguage>();
        public List<LanguageEmployee> languageemployees = new List<LanguageEmployee>();

        //Hàm Initial() khởi tạo dữ liệu ban đầu cho các list
        public void Initial()
        {
            employees.Add(new Employee { employeeID = 1, employeeName = "Nguyễn Thị Cẩm Tú", employeeAge = 18, employeeAdress = "Nghệ An", hiredDate = DateOnly.Parse("2003/01/02"), status = true, departmentID = 1 });
            employees.Add(new Employee { employeeID = 2, employeeName = "Trương Thị Thanh Hòa", employeeAge = 18, employeeAdress = "Nghệ An", hiredDate = DateOnly.Parse("2003/01/02"), status = false, departmentID = 1 });
            employees.Add(new Employee { employeeID = 3, employeeName = "Trần Thu Trang", employeeAge = 18, employeeAdress = "Nghệ An", hiredDate = DateOnly.Parse("2003/01/02"), status = false, departmentID = 2 });
            employees.Add(new Employee { employeeID = 4, employeeName = "Đỗ Thị Thanh Thủy", employeeAge = 18, employeeAdress = "Nghệ An", hiredDate = DateOnly.Parse("2003/01/02"), status = false, departmentID = 2 });
            employees.Add(new Employee { employeeID = 5, employeeName = "Nguyễn Duy Phúc", employeeAge = 18, employeeAdress = "Nghệ An", hiredDate = DateOnly.Parse("2003/01/02"), status = true, departmentID = 2 });
            employees.Add(new Employee { employeeID = 6, employeeName = "Nguyễn Tuấn Ninh", employeeAge = 18, employeeAdress = "Nghệ An", hiredDate = DateOnly.Parse("2003/01/02"), status = true, departmentID = 1 });
            employees.Add(new Employee { employeeID = 7, employeeName = "Nguyễn Xuân Hậu", employeeAge = 18, employeeAdress = "Nghệ An", hiredDate = DateOnly.Parse("2003/01/02"), status = false, departmentID = 1 });
            employees.Add(new Employee { employeeID = 8, employeeName = "Phạm Khải Văn", employeeAge = 18, employeeAdress = "Nghệ An", hiredDate = DateOnly.Parse("2003/01/02"), status = false, departmentID = 2 });
            employees.Add(new Employee { employeeID = 9, employeeName = "Đặng Công Đạt", employeeAge = 18, employeeAdress = "Nghệ An", hiredDate = DateOnly.Parse("2003/01/02"), status = false, departmentID = 2 });
            employees.Add(new Employee { employeeID = 10, employeeName = "Trần Văn Dũng", employeeAge = 18, employeeAdress = "Nghệ An", hiredDate = DateOnly.Parse("2003/01/02"), status = true, departmentID = 1 });
            employees.Add(new Employee { employeeID = 11, employeeName = "Trần Hoàng Long", employeeAge = 18, employeeAdress = "Nghệ An", hiredDate = DateOnly.Parse("2003/01/02"), status = false, departmentID = 3 });
            employees.Add(new Employee { employeeID = 12, employeeName = "Điêu Quý Duy", employeeAge = 18, employeeAdress = "Nghệ An", hiredDate = DateOnly.Parse("2003/01/02"), status = false, departmentID = 3 });
            employees.Add(new Employee { employeeID = 13, employeeName = "Nguyễn Đăng Kiên", employeeAge = 18, employeeAdress = "Nghệ An", hiredDate = DateOnly.Parse("2003/01/02"), status = false, departmentID = 3 });


            departments.Add(new Department { departmentID = 1, departmentName = "CNTT" });
            departments.Add(new Department { departmentID = 2, departmentName = "QTKD" });
            departments.Add(new Department { departmentID = 3, departmentName = "ATTT" });

            programmingLanguages.Add(new ProgrammingLanguage { languageName = "C#", languageID = 1 });
            programmingLanguages.Add(new ProgrammingLanguage { languageName = "Java", languageID = 2 });
            programmingLanguages.Add(new ProgrammingLanguage { languageName = "Python", languageID = 3 });

            languageemployees.Add(new LanguageEmployee { employeeID = 1, languageID = 1 });
            languageemployees.Add(new LanguageEmployee { employeeID = 1, languageID = 2 });
            languageemployees.Add(new LanguageEmployee { employeeID = 3, languageID = 2 });
            languageemployees.Add(new LanguageEmployee { employeeID = 3, languageID = 3});
        }

        //Hàm GetDepartMentWithMinimumEmployees cho phép người dùng nhập vào số nhân viên tối thiểu rồi trả về danh sách department có nhiều hơn hoặc bằng nhân viên
        public void GetDepartMentWithMinimumEmployees()
        {
            Console.Write("Enter Number Of Minimum Employees: ");
            int number = Validate.ValidateNumber("number of minimum employees");
            GetDepartments(number);
            Console.Write("Do you want to continue? (Yes/No): ");
            string option = Validate.ValidateContinueChoice("Yes", "No");
            if (option.ToLower().Equals("yes"))
            {
                GetDepartMentWithMinimumEmployees();
            }
        }

        //Hàm GetDepartments để in ra danh sách deparment có số nhân viên lớn hơn numberOfEmployees
        public void GetDepartments(int numberOfEmployees)
        {
            var departmentsResult = from de in departments
                                    join em in employees on de.departmentID equals em.departmentID into result
                                    where result.Count() >= numberOfEmployees
                                    select new
                                    {
                                        departmentID = de.departmentID,
                                        departmentName = de.departmentName,
                                        numOfEmps = result.Count()
                                    };
            Console.WriteLine("Number of Departments have number of employees >= " + numberOfEmployees + " employees is: " + departmentsResult.Count() + "/" + departments.Count());
            if (departmentsResult.Count() > 0)
            {
                Console.WriteLine("List of Departments have number of employees >= " + numberOfEmployees + " employees:");
                Console.WriteLine("{0,-15} {1,-20} {2,-30}", "Department ID", "Department Name", "Number Of Employees");
                foreach (var department in departmentsResult)
                {
                    Console.WriteLine("{0,-15} {1,-20} {2,-30}", department.departmentID, department.departmentName, department.numOfEmps);
                }
            }
        }

        //Hàm GetEmployeesWorking in ra danh sach nhân viên đang làm việc 
        public void GetEmployeesWorking()
        {
            var employeeResult = from emp in employees
                                 where emp.status == true
                                 join de in departments on emp.departmentID equals de.departmentID
                                 select new
                                 {
                                     employee = emp,
                                     status = "Working",
                                     department = de
                                 };

            Console.WriteLine("Number of Employees who is working: " + employeeResult.Count() + "/" + employees.Count());
            if (employeeResult.Count() > 0)
            {
                Console.WriteLine("List of Employees who is working: ");
                Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -12} {6,-15}", "Employee ID", "Employee Name", "Employee Age", "Employee Address", "Hired Date", "Department", "Status");
                foreach (var e in employeeResult)
                {
                    Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -12} {6,-15}", e.employee.employeeID, e.employee.employeeName, e.employee.employeeAge, e.employee.employeeAdress, e.employee.hiredDate, e.department.departmentName, e.status);
                }
            }
        }
        //Hàm GetEmployees in ra danh sách nhân viên biết ngôn ngữ languageName do người dùng nhập vào
        public void GetEmployees(string languageName)
        {
            var employeesResult = from emla in languageemployees
                                  join emp in employees on emla.employeeID equals emp.employeeID
                                  join de in departments on emp.departmentID equals de.departmentID
                                  join lg in programmingLanguages on emla.languageID equals lg.languageID
                                  where lg.languageName.ToLower() == languageName.ToLower()
                                  select new {
                                              employee = emp, 
                                              languageName = emla,
                                              department = de, 
                                              status = (emp.status) ? "Working" : "Not Working" };

            Console.WriteLine("Number of Employees who is knowing " + languageName + " is: " + employeesResult.Count() + "/" + employees.Count());
            if (employeesResult.Count() > 0)
            {
                Console.WriteLine("List of Employees who is knowing " + languageName + " is: ");
                Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -12} {6,-15}", "Employee ID", "Employee Name", "Employee Age", "Employee Address", "Hired Date", "Department", "Status");

                foreach (var e in employeesResult)
                {
                    Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -12} {6,-15}", e.employee.employeeID, e.employee.employeeName, e.employee.employeeAge, e.employee.employeeAdress, e.employee.hiredDate, e.department.departmentName, e.status);
                }
            }
        }

        //Hàm GetEmployeeByLanguage cho phép người dùng nhập vào ngôn ngữ lập trình và in ra danh sách nhân viên biết ngôn ngữ đó

        public void GetEmployeeByLanguage()
        {
            Console.Write("Enter Programing Language: ");
            bool a = true;
            while(a)
            {
                string lang = Console.ReadLine();
                int count = (from lan in programmingLanguages
                             where lan.languageName.ToLower() == (lang.ToLower())
                             select lan).Count();
                if (count <= 0)
                {
                    Console.WriteLine(lang + " is not exist in Programing Language List!\nPlease enter again!");
                    Console.Write("Enter Programing Language: ");
                }
                else
                {
                    GetEmployees(lang);
                    a= false;
                }

            }

            Console.Write("Do you want to continue? (Yes/No): ");
            string option = Validate.ValidateContinueChoice("Yes", "No");
            if (option.ToLower().Equals("yes"))
            {
                GetEmployeeByLanguage();
            }
        }
        //Hàm GetLanguages để in ra danh sách ngông ngữ mà nhân viên có id do người dùng nhập vào biết
        public void GetLanguages(int employeeid)
        {
            var languageResult = from lanemp in languageemployees
                                 join emp in employees on lanemp.employeeID equals emp.employeeID
                                 join lang in programmingLanguages on lanemp.languageID equals lang.languageID
                                 where emp.employeeID == employeeid
                                 select lang;

            Console.WriteLine("Number of Languages that employee has ID = " + employeeid + " knowing: " + languageResult.Count());
            if (languageResult.Count() > 0)
            {
                Console.WriteLine("List of Languages that employee has ID = " + employeeid + " knowing: ");
                Console.WriteLine("{0,-13} {1,-20}", "STT", "Programming Language");
                int i = 1;
                foreach (var e in languageResult)
                {
                    Console.WriteLine("{0,-13} {1,-20}", i, e.languageName);
                    i++;
                }
            }
        }
        public void GetLanguageWithEmployeeID()
        {
        L1: Console.Write("Enter Employee ID: ");
            int id = Validate.ValidateNumber("Employee ID");
            int count = (from lan in employees
                         where lan.employeeID == id
                         select lan).Count();
            if (count <= 0)
            {
                Console.WriteLine("Employee has ID = " + id + " is not exist in Employee List!\nPlease enter again!");
                goto L1;
            }
            else
            {
                GetLanguages(id);
            }
            Console.Write("Do you want to continue? (Yes/No): ");
            string option = Validate.ValidateContinueChoice("Yes", "No");
            if (option.ToLower().Equals("yes"))
            {
                GetLanguageWithEmployeeID();
            }
        }
        public void GetSeniorEmployee()
        {
            var employeeResult = from emp in employees
                                 join emlan in languageemployees on emp.employeeID equals emlan.employeeID into result
                                 where result.Count() > 1
                                 select new
                                 {
                                     employee = emp,
                                     numOflanguage = result.Count(),
                                     status = (emp.status == true) ? "Working" : "Not Working"
                                 };
            Console.WriteLine("Number of Employees know multiple programming languages: " + employeeResult.Count() + "/" + employees.Count());
            if (employeeResult.Count() > 0)
            {
                Console.WriteLine("List of Employees know multiple programming languages: ");
                Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -20} {6,-15}", "Employee ID", "Employee Name", "Employee Age", "Employee Address", "Hired Date", "Number Of Languages", "Status");
                foreach (var e in employeeResult)
                {
                    Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -20} {6,-15}", e.employee.employeeID, e.employee.employeeName, e.employee.employeeAge, e.employee.employeeAdress, e.employee.hiredDate, e.numOflanguage, e.status);
                }
            }
        }

        //Hàm GetEmployeePaging Phân trang danh sách trước, lọc kết quả theo tên rồi trả về về danh sách nhân viên được sắp xếp
        public void GetEmployeePaging(int pageIndex = 1, int pageSize = 10, string employeeName = null, string order = "ASC")
        {
            var pagingResult = (from emp in employees
                                select emp)
                                     .Skip(pageSize * (pageIndex - 1))
                                     .Take(pageSize);
            if (pagingResult.Count() > 0)
            {
                if (employeeName == String.Empty || employeeName == null ||employeeName.Length ==0)
                {
                    if (order.ToLower().Equals("asc"))
                    {
                        var result = from emp in pagingResult
                                     orderby emp.employeeName ascending
                                     select new
                                     {
                                         employee = emp,
                                         status = (emp.status == true) ? "Working" : "Not Working"
                                     };
                        Console.WriteLine("List of Employees at Page Index = " + pageIndex + " with Page Size = " + pageSize + " is: ");
                        Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -20}", "Employee ID", "Employee Name", "Employee Age", "Employee Address", "Hired Date", "Status");
                        foreach (var e in result)
                        {
                            Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -20} ", e.employee.employeeID, e.employee.employeeName, e.employee.employeeAge, e.employee.employeeAdress, e.employee.hiredDate, e.status);
                        }
                    }
                    if (order.ToLower().Equals("desc"))
                    {
                        var result = from emp in pagingResult
                                     orderby emp.employeeName descending
                                     select new
                                     {
                                         employee = emp,
                                         status = (emp.status == true) ? "Working" : "Not Working"
                                     };
                        Console.WriteLine("List of Employees at Page Index = " + pageIndex + " with Page Size = " + pageSize + " is: ");
                        Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -20}", "Employee ID", "Employee Name", "Employee Age", "Employee Address", "Hired Date", "Status");
                        foreach (var e in result)
                        {
                            Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -20} ", e.employee.employeeID, e.employee.employeeName, e.employee.employeeAge, e.employee.employeeAdress, e.employee.hiredDate, e.status);
                        }
                    }

                }
                else
                {
                    if (order.ToLower().Equals("asc"))
                    {
                        var result = from emp in pagingResult
                                     orderby emp.employeeName ascending
                                     where emp.employeeName.Contains(employeeName)
                                     select new
                                     {
                                         employee = emp,
                                         status = (emp.status == true) ? "Working" : "Not Working"
                                     };
                        if (result.Count() > 0)
                        {
                            Console.WriteLine("List of Employees at Page Index = " + pageIndex + " with Page Size = " + pageSize + " is: ");
                            Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -20}", "Employee ID", "Employee Name", "Employee Age", "Employee Address", "Hired Date", "Status");
                            foreach (var e in result)
                            {
                                Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -20} ", e.employee.employeeID, e.employee.employeeName, e.employee.employeeAge, e.employee.employeeAdress, e.employee.hiredDate, e.status);
                            }
                        }
                        else
                        {
                            Console.WriteLine("The Searched Employee is not exist!");
                        }

                    }
                    if (order.ToLower().Equals("desc"))
                    {
                        var result = from emp in pagingResult
                                     orderby emp.employeeName descending
                                     where emp.employeeName.Contains(employeeName)
                                     select new
                                     {
                                         employee = emp,
                                         status = (emp.status == true) ? "Working" : "Not Working"
                                     };
                        if (result.Count() > 0)
                        {
                            Console.WriteLine("List of Employees at Page Index = " + pageIndex + " with Page Size = " + pageSize + " is: ");
                            Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -20}", "Employee ID", "Employee Name", "Employee Age", "Employee Address", "Hired Date", "Status");
                            foreach (var e in result)
                            {
                                Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -20} ", e.employee.employeeID, e.employee.employeeName, e.employee.employeeAge, e.employee.employeeAdress, e.employee.hiredDate, e.status);
                            }
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("The employee list not exist because out of range!");
            }

        }
        //Hàm GetEmployeByPaging dùng để cho phép người dùng nhập vào pagesize, pageindex, tên cần tim,thứ tự sắp xếp và in ra danh sách phù hợp
        public void GetEmployeByPaging()
        {
            Console.Write("Enter Page Size: ");
            int pagesize = Validate.ValidateNumber("Page Size");
            Console.Write("Enter Page Index: ");
            int pageindex = Validate.ValidateNumber("Page Index");
            Console.Write("Enter Employee Name: ");
            string name = Console.ReadLine();
            Console.Write("Order List By Ascending Or Descnding? (ASC or DESC): ");
            string oby = Validate.ValidateContinueChoice("ASC", "DESC");
            GetEmployeePaging(pageindex, pagesize, name, oby);
            Console.Write("Do you want to continue? (Yes/No): ");
            string option = Validate.ValidateContinueChoice("Yes", "No");
            if (option.ToLower().Equals("yes"))
            {
                GetEmployeByPaging();
            }

        }
        //Hàm GetDepartments để in ra danh sách tất cả department và nhân ciên của nó
        public void GetDepartments()
        {
            Console.WriteLine("List Of All Departments With Employees Belong To Each Department");
            foreach (Department de in departments)
            {
                var employeeResurt = from e in employees
                                     where e.departmentID == de.departmentID
                                     select new
                                     {
                                         employee = e,
                                         status = e.status == true ? "Working" : "Not Working"
                                     };
                Console.WriteLine("Department Name: " + de.departmentName);
                Console.WriteLine("Number of emloyees belong to department " + de.departmentName + " is: " + employeeResurt.Count() + "/" + employees.Count());

                if (employeeResurt.Count() > 0)
                {
                    Console.WriteLine("List of employees belong to department " + de.departmentName);
                    Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -20}", "Employee ID", "Employee Name", "Employee Age", "Employee Address", "Hired Date", "Status");
                    foreach (var e in employeeResurt)
                    {
                        Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -20} ", e.employee.employeeID, e.employee.employeeName, e.employee.employeeAge, e.employee.employeeAdress, e.employee.hiredDate, e.status);
                    }
                }
                Console.WriteLine();
            }
        }
        //Hàm AddEmployee dùng để cho phép người dùng nhập vào thông tin nhân viên
        public void AddEmployee()
        {
            Console.WriteLine("Enter Employee Information:");
            Employee employee = new Employee();
            Console.Write("Enter Employee ID: ");
            int eid = 0;
            bool a = true;
            while(a)
            {
                eid = Validate.ValidateNumber("Employee ID");
                int countID = employees.Where(x => x.employeeID == eid).Count();
                if(countID > 0)
                {
                    Console.WriteLine("Duplicate Employee ID! Enter Again!");
                    Console.Write("Enter Employee ID: ");
                }
                else
                    a= false;
            }    
            
            employee.employeeID = eid;

            Console.Write("Enter Employee Name: ");
            employee.employeeName = Validate.ValidateName();

            Console.Write("Enter Employee Age: ");
            employee.employeeAge = Validate.ValidateNumber("Employee Age");

            Console.Write("Enter Employee Adress: ");
            employee.employeeAdress = Console.ReadLine();

            Console.Write("Enter Employee Hired Date (dd/MM/yyyy): ");
            employee.hiredDate = Validate.ValidateDate("Hired Date");

            Console.WriteLine("Enter Employee Status: ");
            Console.WriteLine("1. Working ");
            Console.WriteLine("2. Not Working");
            Console.Write("Status Of Employee (Enter number 1 or 2): ");
            int status = Validate.ValidateChoice(1, 2);
            if (status == 1)
            {
                employee.status = true;
            }
            else
            {
                employee.status = false;
            }
            Console.WriteLine("Choose Employee Department ID: ");
            foreach (Department de in departments)
            {
                Console.WriteLine(de.departmentID + ". " + de.departmentName);
            }
            Console.Write("Department Of Employee (Enter number only): ");
            int did = 0;
            bool b = true;
            while(b)
            {
                
                did = Validate.ValidateNumber("Department");
                int count = departments.Where(x => x.departmentID == did).Count();
                if(count <= 0)
                {
                    Console.WriteLine("Invalid Department ID? Please Choose Department Of Employee!");
                    Console.Write("Department Of Employee (Enter number only): ");
                }
                else
                    b = false;
            }    

            employee.departmentID = did;
            employees.Add(employee);
            Console.WriteLine("Employee Information is added!");
            Console.Write("Do you want to continue?(Yes/No): ");
            string option = Validate.ValidateContinueChoice("Yes", "No");
            if (option.ToLower().Equals("yes"))
            {
                AddEmployee();
            }
        }
        //Hàm AddProgramLanguage cho phép người dùng nhập vào thông tin của Program Language
        public void AddProgramLanguage()
        {
            Console.WriteLine("Enter Program Language Information:");
            Console.Write("Enter Program Language ID: ");
            ProgrammingLanguage language = new ProgrammingLanguage();
            int lid = 0;
            bool a = true;
            while (a)
            {
                lid = Validate.ValidateNumber("Program Language ID");
                int countID = programmingLanguages.Where(x => x.languageID == lid).Count();
                if (countID > 0)
                {
                    Console.WriteLine("Duplicate Program Language ID! Enter Again!");
                    Console.Write("Enter Program Language ID: ");
                }
                else
                    a = false;
            }
            language.languageID = lid;

            Console.Write("Enter Program Language Name: ");
            string name = null;
            bool b = true;
            while (b)
            {
                name = Console.ReadLine();
                int count = programmingLanguages.Where(x => x.languageName.ToLower() == name).Count();
                if (count > 0)
                {
                    Console.WriteLine("Duplicate Program Language Name! Enter Again!");
                    Console.Write("Enter Program Language Name: ");
                }
                else
                    b = false;
            }
            language.languageName = name;

            programmingLanguages.Add(language);
            Console.WriteLine("Program Language Information is added!");

            Console.Write("Do you want to continue?(Yes/No): ");
            string option = Validate.ValidateContinueChoice("Yes", "No");
            if (option.ToLower().Equals("yes"))
            {
                AddProgramLanguage();
            }
        }
        //Hàm AddDepartment cho phép người dùng nhập vào thông tin của Department
        public void AddDepartment()
        {
            Console.WriteLine("Enter Department Information:");
            Console.Write("Enter Department ID: ");
            Department department = new Department();
            int did = 0;
            bool a = true;
            while (a)
            {
                did = Validate.ValidateNumber("Department ID");
                int countID = departments.Where(x => x.departmentID == did).Count();
                if (countID > 0)
                {
                    Console.WriteLine("Duplicate Department ID! Enter Again!");
                    Console.Write("Enter Department ID: ");
                }
                else
                    a = false;
            }
            department.departmentID = did;

            Console.Write("Enter Department Name: ");
            string name = null;
            bool b = true;
            while (b)
            {
                name = Console.ReadLine();
                int count = departments.Where(x => x.departmentName.ToLower() == name).Count();
                if (count > 0)
                {
                    Console.WriteLine("Duplicate Department Name! Enter Again!");
                    Console.Write("Enter Department Name: ");
                }
                else
                    b = false;
            }
            department.departmentName = name;

            departments.Add(department);
            Console.WriteLine("Department Information is added!");

            Console.Write("Do you want to continue?(Yes/No): ");
            string option = Validate.ValidateContinueChoice("Yes", "No");
            if (option.ToLower().Equals("yes"))
            {
                AddDepartment();
            }
        }

        //Hàm AddSkill cho phép người dùng nhập thông tin employeeID biết ngôn ngữ languageID
        public void AddSkill()
        {
            Console.WriteLine("Enter Skill Information:");
            LanguageEmployee skill = new LanguageEmployee();
            Console.Write("Enter Employee ID: ");
            int eid = 0;
            bool a = true;
            while (a)
            {
                eid = Validate.ValidateNumber("Employee ID");
                int countID = employees.Where(x => x.employeeID == eid).Count();
                if (countID <= 0)
                {
                    Console.WriteLine("Employee ID does not exist! Enter Again!");
                    Console.Write("Enter Employee ID: ");
                }
                else
                    a = false;
            }


            Console.Write("Enter Program Language ID: ");
            int lid = 0;
            bool b = true;
            while (b)
            {
                lid = Validate.ValidateNumber("Program Language ID");
                int countID = programmingLanguages.Where(x => x.languageID == lid).Count();
                if (countID <= 0)
                {
                    Console.WriteLine("Program Language ID does not exist! Enter Again!");
                    Console.Write("Enter Program Language ID: ");
                }
                else
                    b = false;
            }


            var emskill =  from emla in languageemployees
                           where emla.employeeID == eid && emla.languageID == lid
                           select emla;
            if (emskill.Count() > 0)
            {
                Console.WriteLine("Employee already known this program language!");
            }
            else
            {

                skill.employeeID = eid;
                skill.languageID = lid;
                languageemployees.Add(skill);
                Console.WriteLine("Employee and Program Language Information is added!");
            }


            Console.Write("Do you want to continue?(Yes/No): ");
            string option = Validate.ValidateContinueChoice("Yes", "No");
            if (option.ToLower().Equals("yes"))
            {
                AddSkill();
            }

        }

    }
}
