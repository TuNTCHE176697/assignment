﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace NPL.M.A010.Exercise.MailManagement
{
    internal class MailManagement
    {
        List<OutLookMail> outlookMails = new List<OutLookMail>();

        //hàm AddNewEmail dùng để thực hiện thêm nội dung email
        public void AddNewEmail()
        {
            OutLookMail mail = new OutLookMail();
            Console.WriteLine("========= Enter email information =========");
            Console.Write("From: ");
            mail.From = Validate.ValidateFromEmail();
            Console.Write("To: ");
            mail.To = Validate.ValidateToEmail();
            Console.Write("Cc: ");
            mail.Cc = Validate.ValidateToEmail();
            Console.Write("Subject: ");
            mail.Subject = Console.ReadLine();
            Console.Write("Attachment: ");
            mail.AttachmentPath = Console.ReadLine();
            Console.WriteLine("Mail body: ");
            mail.MailBody = Validate.InputMailBody();
            Console.Write("Is Important (Yes or No): ");
            string important = Validate.ValidateContinueChoice("Yes", "No");
            if (important.Equals("Yes") || important.Equals("yes"))
            {
                mail.IsImportant = true;
                Console.Write("Password: ");
                string password = Console.ReadLine();
                mail.EncryptedPassword = Validate.EncryptPassword(password);
            }
            else
            {
                mail.IsImportant = false;
                mail.EncryptedPassword = null;
            }

            Console.WriteLine("========= Mailing Menu =========");
            Console.WriteLine("1. Send.");
            Console.WriteLine("2. Draft.");
            Console.WriteLine("3. Re-enter.");
            Console.WriteLine("4. Main menu.");
            Console.Write("Enter Menu Option Number: ");
            int option = Validate.ValidateChoice(1, 4);
            if (option == 1)
            {
                mail.Status = MailStatus.Sent;
                mail.SentDate = DateTime.Now;
                outlookMails.Add(mail);
                Console.WriteLine("Email sent successfully!");
                WriteToXMLFile();
            }
            else if (option == 2)
            {
                mail.Status = MailStatus.Draft;
                mail.SentDate = null;
                outlookMails.Add(mail);
                Console.WriteLine("Email daft successfully!");
                WriteToXMLFile();
            }
            else if (option == 3)
            {
                AddNewEmail();
            }

            Console.WriteLine("====================================");
            Console.Write("Would you like to sent an other?(Yes or No)");
            string choice = Validate.ValidateContinueChoice("Yes", "No");
            if (choice.Equals("Yes") || choice.Equals("yes"))
            {
                AddNewEmail();
            }

        }
        //Hàm DisplaySentEmail để xuất ra danh sách các email đã gửi với 3 trường From, To, Subject
        public void DisplaySentEmail()
        {
            List<OutLookMail> sentMails = new List<OutLookMail>();
            foreach (OutLookMail mail in outlookMails)
            {
                if (mail.Status == MailStatus.Sent)
                {
                    sentMails.Add(mail);
                }
            }
            Validate.SortMailBySubject(sentMails);
        }

        //Hàm DisplayDraftEmail để xuất ra danh sách các email đã gửi với 3 trường From, To, Subject
        public void DisplayDraftEmail()
        {
            List<OutLookMail> sentMails = new List<OutLookMail>();
            foreach (OutLookMail mail in outlookMails)
            {
                if (mail.Status == MailStatus.Draft)
                {
                    sentMails.Add(mail);
                }
            }
            Validate.SortMailBySubject(sentMails);
        }

        //Hàm WriteToXMLFile để ghi nội dung danh sách email vào file xml
        public void WriteToXMLFile()
        {
            XmlDocument xmlDoc;
            string path = Environment.CurrentDirectory+"\\mail.xml";
            //xmlDoc.Load(path);
            //Khai báo nút khai báo tài liệu để lưu trữ <?xml version="1.0" encoding="utf-8" ?> 
            XmlNode decNode;
            //Khai báo các phần tử của tài liệu
            XmlElement outlookemail, email, from, to, address, cc, subject, attachment, body, isimportant, password, status, sentDate;
            //Tạo mới đối tượng XmlDocument
            xmlDoc = new XmlDocument();
            //Tạo mới nút khai báo tài liệu
            decNode = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            //Thêm nút khai báo vào tài liệu
            xmlDoc.AppendChild(decNode);

            outlookemail = xmlDoc.CreateElement("OutlookEmail");

            foreach (OutLookMail mail in outlookMails)
            {
                email = xmlDoc.CreateElement("Mail"); //Tạo mới Element Mail

                from = xmlDoc.CreateElement("From"); //Tạo mới Element 
                from.InnerText = mail.From; //Gán giá trị cho Element

                to = xmlDoc.CreateElement("To");
                foreach (string toemail in mail.To)
                {
                    address = xmlDoc.CreateElement("Address");
                    address.InnerText = toemail;
                    to.AppendChild(address);
                }

                cc = xmlDoc.CreateElement("Cc");
                foreach (string ccemail in mail.Cc)
                {
                    address = xmlDoc.CreateElement("Address");
                    address.InnerText = ccemail;
                    cc.AppendChild(address);
                }

                subject = xmlDoc.CreateElement("Subject");
                subject.InnerText = mail.Subject;

                attachment = xmlDoc.CreateElement("Attachment");
                attachment.InnerText = mail.AttachmentPath;

                body = xmlDoc.CreateElement("Body");
                body.InnerText = mail.MailBody;

                isimportant = xmlDoc.CreateElement("IsImportant");
                if (mail.IsImportant)
                {
                    isimportant.InnerText = "Yes";

                }
                else
                {
                    isimportant.InnerText = "No";
                }
                password = xmlDoc.CreateElement("Password");
                password.InnerText = mail.EncryptedPassword;
                status = xmlDoc.CreateElement("Status");
                status.InnerText = mail.Status.ToString();

                sentDate = xmlDoc.CreateElement("SentDate");
                sentDate.InnerText = mail.SentDate.ToString();

                email.AppendChild(from);
                email.AppendChild(to);
                email.AppendChild(cc);
                email.AppendChild(subject);
                email.AppendChild(attachment);
                email.AppendChild(body);
                email.AppendChild(isimportant);
                email.AppendChild(password);
                email.AppendChild(status);
                email.AppendChild(sentDate);

                outlookemail.AppendChild(email);
            }

            xmlDoc.AppendChild(outlookemail);
            xmlDoc.Save(path);
        }

        //Hàm DisplayFromXmlFile dùng để trả nội dung file xml về màn hình Console
        public void DisplayFromXmlFile()
        {
            Console.WriteLine("Please Enter mail.xml to read email list from file mail.xml: ");
            string nameFile = Validate.ValidateInputFile();
            XmlDocument xmlDoc = new XmlDocument();
            string path = Environment.CurrentDirectory + "\\"+nameFile;
            xmlDoc.Load(path);
            xmlDoc.Save(Console.Out);
        }

    }
}
