﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A010.Exercise.MailManagement
{
    internal class OutLookMail
    {
        public string From { get; set; }
        public List<string> To { get; set; }
        public List<string> Cc { get; set; }
        public string Subject { get; set; }
        public string AttachmentPath { get; set; }
        public string MailBody { get; set; }
        public bool IsImportant { get; set; }
        public string EncryptedPassword { get; set; }
        public DateTime? SentDate { get; set; }
        public MailStatus Status { get; set; }

        public OutLookMail()
        {
        }

        public OutLookMail(string from, List<string> to, List<string> cc, string subject, string attachmentPath, string mailBody, bool isImportant, string? encryptedPassword, MailStatus status, DateTime? sendDate)
        {
            this.From = from;
            this.To = to;
            this.Cc = cc;
            this.Subject = subject;
            this.AttachmentPath = attachmentPath;
            this.MailBody = mailBody;
            this.IsImportant = isImportant;
            this.EncryptedPassword = encryptedPassword;
            this.SentDate = sendDate;
            this.Status = status;
        }
    }

    public enum MailStatus
    {
        Draft,
        Sent
    }

    
}

