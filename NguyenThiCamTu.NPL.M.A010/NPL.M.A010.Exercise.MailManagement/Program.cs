﻿using NPL.M.A010.Exercise.MailManagement;
using System.Text;
Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;

Console.WriteLine("========= Assignment 11 - OutLook Emulator =========");
int choice;
MailManagement mailManagement = new MailManagement();

do
{
    Console.WriteLine("Please select the admin area you require:");
    Console.WriteLine("1. New Mail.");
    Console.WriteLine("2. Sent.");
    Console.WriteLine("3. Draft");
    Console.WriteLine("4. Display the email list from XML file");
    Console.WriteLine("5. Exit.");
    Console.Write("Enter Menu Option Number: ");
    choice = Validate.ValidateChoice(1, 5);

    switch (choice)
    {
        case 1:
            mailManagement.AddNewEmail();
            break;

        case 2:
            mailManagement.DisplaySentEmail();
            break;
        case 3:
            mailManagement.DisplayDraftEmail();
            break;
        case 4:
            mailManagement.DisplayFromXmlFile();
            break;
        case 5:
            Console.WriteLine("End Program!");
            break;
    }
}
while (choice > 0 && choice < 4);
Console.ReadKey();
