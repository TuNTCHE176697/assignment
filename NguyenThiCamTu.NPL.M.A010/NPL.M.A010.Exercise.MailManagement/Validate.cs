﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace NPL.M.A010.Exercise.MailManagement
{
    internal class Validate
    {
        //Hàm ValidateChoice dùng để ép người dùng nhập vào lựa chọn là kiểu số nguyên trong phạm vi yêu cầu
        public static int ValidateChoice(int min, int max)
        {
            while (true)
            {
                string StringInput = Console.ReadLine();
                try
                {
                    if (string.IsNullOrEmpty(StringInput))
                    {
                        Console.Write("The option must not be empty!\nEnter a number again: ");
                    }
                    else
                    {
                        int IntegerInput = int.Parse(StringInput);
                        if (IntegerInput < min || IntegerInput > max)
                        {
                            Console.WriteLine("Invalid number! Please enter an number from " + min + " to " + max);
                            Console.Write("Enter a number from " + min + " to " + max + ": ");
                        }
                        else return IntegerInput;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid number! Please enter an number from " + min + " to " + max);
                    Console.Write("Enter a number from " + min + " to " + max + ": ");
                }
            }
        }
        //Hàm ValidateContinueChoice ép người dùng nhập vào string a hay b
        public static string ValidateContinueChoice(string a, string b)
        {
            while (true)
            {
                string choice = Console.ReadLine();
                if (choice == a || choice == b || choice == a.ToLower() || choice == b.ToLower())
                {
                    return choice;
                }
                else
                {
                    Console.WriteLine("Invalid Choice!Please enter again");
                    Console.Write("Enter your option (" + a + " or " + b + "): ");
                }
            }
        }

        //Hàm ValidateEmail() để validate email đúng định dạng @fsoft.com.vn và không được null
        public static bool ValidateEmail(string email)
        {
            string regex = "^[a-zA-Z0-9._%+-]+@fsoft\\.com\\.vn$";
            bool a = false;
            if(email != String.Empty && Regex.IsMatch(email,regex) == true)
            {
                a = true;
            }
            return a;
        }

        //Hàm ValidateFromEmail bắt người dùng nhập vào chỉ 1 sender email và có định dạng @fsoft.com.vn và không được null
        public static String ValidateFromEmail()
        {
            while (true)
            {
                string fromEmail = Console.ReadLine();
                if (ValidateEmail(fromEmail))
                {
                    return fromEmail;
                }
                else
                {
                    Console.Write("Email format is not our company\nAnd Only One Sender Email!\nPlease Enter sender email again!: ");
                }
            }
        }

        //Hàm ValidateToEmail bắt người dùng nhập vào các receiver email và có định dạng @fsoft.com.vn và không được null, các email cách nhau bởi dấu ,

        public static List<String> ValidateToEmail()
        {
            while (true)
            {
                string toEmail = Console.ReadLine();
                string[] toEmailList = toEmail.Split(",", StringSplitOptions.TrimEntries);
                bool check = true;
                List<string> emails = new List<string>();
                foreach (string email in toEmailList)
                {
                    if (!ValidateEmail(email))
                    {
                        check = false; 
                        break;
                    }
                    else
                        emails.Add(email);
                }
                if(check)
                {
                    return emails;
                }
                else
                {
                    Console.Write("Email format is not our company\nPlease Enter receiver email again! (Each email separated by , ): ");

                }

            }
        }


        //Hàm InputMailBody cho phép người dùng nhập dữ liệu từng hàng đến khi ấn End để kết thúc
        public static string InputMailBody()
        {
            string retString = "";
            
            do
            {
                ConsoleKeyInfo readKeyResult = Console.ReadKey(true);

                // handle Ctrl + Enter
                if (readKeyResult.Key == ConsoleKey.End)
                {
                    Console.WriteLine();
                    return retString;
                }

                // handle Enter
                if (readKeyResult.Key == ConsoleKey.Enter)
                {
                    retString += Environment.NewLine;
                    Console.WriteLine();
                }

                else
                // handle all other keypresses
                {
                    retString += readKeyResult.KeyChar;
                    Console.Write(readKeyResult.KeyChar);
                }
            }
            while (true);
        }

        //Hàm GenerateSalt để tạo salt ngẫu nhiên
        public static string GenerateSalt()
        {
            //Tạo mảng byte ngẫu nhiên để làm salt
            byte[] saltBytes = new byte[16];
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(saltBytes);
            }
            //Chuyển mảng byte thành chuỗi Base64 và trả về
            return Convert.ToBase64String(saltBytes);
        }

        //Hàm EncryptPassword dùng để mã hóa mật khẩu
        public static string EncryptPassword(string password)
        {
            // Tạo một đối tượng SHA-256 để băm mật khẩu
            using (SHA256 sha256 = SHA256.Create())
            {
                string salt = GenerateSalt();
                // Chuyển salt và mật khẩu thành mảng byte
                byte[] saltBytes = Convert.FromBase64String(salt);
                byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(password + salt);
                // Băm mảng byte chứa salt và mật khẩu
                byte[] hashedBytes = sha256.ComputeHash(passwordBytes);
                // Chuyển kết quả băm thành chuỗi Base64 và trả về
                return Convert.ToBase64String(hashedBytes);
            }
        }

        //Hàm SortMailBySubject sắp xếp các email theo Subject
        public static void  SortMailBySubject(List<OutLookMail> emailList)
        {
            emailList.Sort((email1, email2) => email1.Subject.CompareTo(email2.Subject));
            Console.WriteLine("{0,-20} {1,-20} {2,-50}","From", "To", "Subject");
            foreach (OutLookMail email in emailList)
            {
                foreach(string e in email.To)
                {
                    Console.WriteLine("{0,-20} {1,-20} {2,-50}", email.From, e,email.Subject);
                }    
            }
        }

        //Hàm ValidateInputFile để kiểm tra xem người dùng nhập vào có phải là mail.xml hay không
        public static string ValidateInputFile()
        {
            while (true)
            {
                string path = Console.ReadLine();
                if (path.Equals("mail.xml"))
                {
                    return path;
                }
                else
                {
                    Console.Write("Please Enter Only mail.xml!\nPlease Enter xml file name again!: ");
                }
            }
        }

       
    }
}
