﻿class ConvertToBinary
{
    //Hàm CheckInput ép người dùng nhập vào số nguyên
    public static int CheckInput()
    {
        Console.Write("Enter an integer number: ");
        while (true)
        {
            string StringInput = Console.ReadLine();
            try
            {
                if (string.IsNullOrEmpty(StringInput))
                {
                    Console.Write("The number must not be empty!\nEnter an integer number again: ");
                }
                else
                {
                    int IntegerInput = int.Parse(StringInput);
                    return IntegerInput;
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid number!");
                Console.Write("Enter an number: ");
            }

        }
    }

    public static void Main(string[] args)
    {
        int DecimalNumber = CheckInput();

        //Hàm Convert.ToString(a,2) để chuyển a từ thập phân sang nhị phân
        string BinaryString = Convert.ToString(DecimalNumber, 2);

        Console.WriteLine("Decimal number: " + DecimalNumber);
        Console.WriteLine("Convert to binary: " + BinaryString);
        Console.ReadKey();
    }

}
