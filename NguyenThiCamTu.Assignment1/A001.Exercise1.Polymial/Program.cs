﻿class Polynomial
{
    //Hàm CheckInput để ép người dùng nhập vào phải là kiểu số
    public static float CheckInput()
    {
        Console.Write("Enter a number: ");
        while (true)
        {
            string StringInput = Console.ReadLine();
            try
            {
                if (string.IsNullOrEmpty(StringInput))
                {
                    Console.Write("The number must not be empty!\nEnter number again: ");
                }
                else
                {
                    float FloatInput = float.Parse(StringInput);
                    return FloatInput;
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid number!");
                Console.Write("Enter a number: ");
            }

        }
    }
    public static void Main(string[] args)
    {
        float a = CheckInput();
        float b = CheckInput();
        float c = CheckInput();
        float x1, x2;
        Console.WriteLine("Quadratic equation: " + a + "x^2 + " + b + "x + " + c);
        if (a == 0)
        {
            if (b == 0)
            {
                if (c == 0)
                {
                    Console.WriteLine("Equation has infinitely solutions!");
                }
                else
                {
                    Console.WriteLine("Equation has no solution!");
                }
            }
            else
            {
                x1 = -(c / b);
                Console.WriteLine("Equation has a unique solution : x = " + x1);
            }
        }
        else
        {
            float delta = (b * b) - 4 * a * c;
            if (delta < 0)
            {
                Console.WriteLine("Equation has no solution!");
            }
            else if (delta == 0)
            {
                x1 = -(b / (2 * a));
                Console.WriteLine("Equation has double solution: x1 = x2 = " + x1);
            }
            else
            {
                x1 = (float)((-b + (Math.Sqrt(delta))) / (2 * a));
                x2 = (float)((-b - (Math.Sqrt(delta))) / (2 * a));
                Console.WriteLine("Equation has 2 solutions: x1 = " + x1 + " and x2 = " + x2);
            }
        }
        Console.ReadKey();
    }
}
