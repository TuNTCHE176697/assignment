﻿using System.Security.Cryptography;

class GCDAnArray
{
    //Hàm CheckInputPositive() ép người dùng nhập kích thước mảng là số nguyên dương
    public static int CheckInputPositive()
    {
        while (true)
        {
            string StringInput = Console.ReadLine();
            try
            {
                if (string.IsNullOrEmpty(StringInput))
                {
                    Console.Write("The number must not be empty!\nEnter an integer number again: ");
                }
                else
                {
                    int IntegerInput = int.Parse(StringInput);
                    do
                    {
                        if (IntegerInput <= 0)
                        {
                            Console.Write("The number must be positive!\n Enter again: ");
                            IntegerInput = CheckInputPositive();
                        }

                    } while (IntegerInput <= 0);
                    return IntegerInput;

                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid number!");
                Console.Write("Enter an number: ");
            }

        }
    }

    //Hàm CheckInputElements() để ép người dùng nhập giá trị phần tử trong mảng là số nguyên
    public static int CheckInputElements()
    {
        while (true)
        {
            string StringInput = Console.ReadLine();
            try
            {
                if (string.IsNullOrEmpty(StringInput))
                {
                    Console.Write("The number must not be empty!\nEnter an integer number again: ");
                }
                else
                {
                    int IntegerInput = int.Parse(StringInput);
                    return IntegerInput;

                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid number!");
                Console.Write("Enter an number: ");
            }

        }
    }

    //Hàm MinArray để tìm giá trị nhỏ nhất trong mảng
    public static int MinArray(int[] arr, int n)
    {
        int min = arr[0];
        for (int i = 1; i < n; i++)
        {
            if (arr[i] < min)
                min = arr[i];
        }
        return min;
    }

    //Hàm GCDArray để tìm ra ước chung lớn nhất của mảng
    public static int GCDArray(int[]arr, int n)
    {
        int min = MinArray(arr, n);

        //Vòng lặp For chạy từ phần tử bé nhất trong mảng về 1 để tìm ra số nguyên sao cho tất cả các phần tử trong mảng đều chia hết
        for(int i = min; i >= 1; i--)
        {
            if (IsGcdArray(arr,n,i) == true)
            {
                return i;
            }
        }
        return 1;
    }

    //Hàm IsGcdArray để kiểm tra xem 1 số có phải ước chung của mảng hay không
    public static bool IsGcdArray(int[] arr, int n, int gcd)
    {
        for(int i = 0; i < n; i++)
        {
            if (arr[i] % gcd != 0)
            {
                return false;
            }
        }
        return true;
    }
    public static void Main(string[] args)
    {
        Console.Write("Enter number of elements in array: ");
        int a = CheckInputPositive();
        int[] arr = new int[a];
        Console.WriteLine("Input elements into array:");

        //Vòng lặp for để nhập giá trị từng phần tử trong mảng
        for (int i = 0; i < a; i++)
        {
            Console.Write("arr[" + (i + 1) + "] = ");
            arr[i] = CheckInputElements();
        }
        Console.Write("Array inputted is: [ ");

        //Vòng lặp For để in ra các phần tử trong mảng
        for (int i = 0; i < a; i++)
        {
            Console.Write(arr[i] + " ");
        }
        Console.WriteLine("]");
        int gcd = MinArray(arr, a);
        Console.Write("Greatest common divisor of [ ");
        for (int i = 0; i < a; i++)
        {
            Console.Write(arr[i] + " ");
        }
        Console.Write("] is: " + gcd);
    }

}