﻿class Fibonacci
{
    //Hàm CheckInput để ép người dùng nhập vào số nguyên dương
    public static int CheckInput()
    {
        Console.Write("Enter number of numbers of Fibonnaci : ");
        while (true)
        {
            string StringInput = Console.ReadLine();
            try
            {
                if (string.IsNullOrEmpty(StringInput))
                {
                    Console.Write("The number must not be empty!\nEnter an integer number again: ");
                }
                else
                {
                    int IntegerInput = int.Parse(StringInput);
                    do
                    {
                        if (IntegerInput < 0)
                        {
                            Console.Write("The number must be positive!");
                            IntegerInput = CheckInput();
                        }

                    } while (IntegerInput < 0);
                    return IntegerInput;

                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid number!");
                Console.Write("Enter an number: ");
            }

        }
    }

    //Hàm FibonacciMethod để tính toán dãy Fibonacci bắt đầu từ số 1
    public static int FibonacciMethod(int n)
    {
        if (n < 0)
        {
            return -1;
        }
        else if (n == 0 || n == 1)
        {
            return 1;
        }
        else
        {
            return FibonacciMethod(n - 1) + FibonacciMethod(n - 2);
        }
    }

    public static void Main(string[] args)
    {
        int FibonacciLength = CheckInput();
        Console.WriteLine("The " + FibonacciLength + " first number of Fibonacci: ");
        for (int i = 0; i < FibonacciLength; i++)
        {
            Console.WriteLine(FibonacciMethod(i));
        }
        Console.ReadKey();

    }


}
