﻿class PrimePositive
{
    //Hàm CheckInput để ép người dùng nhập vào số nguyên dương
    public static int CheckInput()
    {
        Console.Write("Enter a positive number : ");
        while (true)
        {
            string StringInput = Console.ReadLine();
            try
            {
                if (string.IsNullOrEmpty(StringInput))
                {
                    Console.Write("The number must not be empty!\nEnter an integer number again: ");
                }
                else
                {
                    int IntegerInput = int.Parse(StringInput);
                    do
                    {
                        if (IntegerInput <= 0)
                        {
                            Console.Write("The number must be positive!");
                            IntegerInput = CheckInput();
                        }

                    } while (IntegerInput <= 0);
                    return IntegerInput;

                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid number!");
                Console.Write("Enter an number: ");
            }

        }
    }
    public static void Main(string[] args)
    {
        int a = CheckInput();
        int count = 0;
        
        //Vòng lặp for để tính số ước nguyên dương của số người dùng nhập vào
        for(int i = 1; i <= a; i++)
        {
            if(a % i == 0)
            {
                count++;
            }
 
        }
        if(count == 2)
        {
            Console.WriteLine(a + " is a prime number");
        }
        else
        {
            Console.WriteLine(a + " is NOT prime number");
        }
    }
}