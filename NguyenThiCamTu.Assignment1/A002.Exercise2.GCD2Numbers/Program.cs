﻿class GDC
{
    //Hàm ép người dùng nhập vào là số nguyên
    public static int CheckInputElements()
    {
        while (true)
        {
            string StringInput = Console.ReadLine();
            try
            {
                if (string.IsNullOrEmpty(StringInput))
                {
                    Console.Write("The number must not be empty!\nEnter an integer number again: ");
                }
                else
                {
                    int IntegerInput = int.Parse(StringInput);
                    return IntegerInput;

                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid number!");
                Console.Write("Enter an number: ");
            }

        }
    }

    //Hàm GCD trả về ước chung lớn nhất 2 số nguyên
    public static int GCD(int number1, int number2)
    {
        int remainder;
        while(number2 > 0)
        {
            remainder = number1 % number2;
            number1 = number2;
            number2 = remainder;
        }
        return number1;
    }
    public static void Main(string[] args)
    {
        Console.Write("Enter the first number: ");
        int a = CheckInputElements();
        Console.Write("Enter the second number: ");
        int b = CheckInputElements();
        int gcd = GCD(Math.Abs(a), Math.Abs(b));
        Console.WriteLine("The greatest common dividor of " + a + " and " + b + " is: " + gcd);
        Console.ReadKey();
    }
}