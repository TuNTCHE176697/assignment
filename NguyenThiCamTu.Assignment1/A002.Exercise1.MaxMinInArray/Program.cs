﻿class MaxMinArray
{
    //Hàm CheckInputPositive để ép người dùng nhập kích thước mảng là 1 số nguyên dương
    public static int CheckInputPositive()
    {
        while (true)
        {
            string StringInput = Console.ReadLine();
            try
            {
                if (string.IsNullOrEmpty(StringInput))
                {
                    Console.Write("The number must not be empty!\nEnter an integer number again: ");
                }
                else
                {
                    int IntegerInput = int.Parse(StringInput);
                    do
                    {
                        if (IntegerInput <= 0)
                        {
                            Console.Write("The number must be positive!\n Enter again: ");
                            IntegerInput = CheckInputPositive();
                        }

                    } while (IntegerInput <= 0);
                    return IntegerInput;

                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid number!");
                Console.Write("Enter an number: ");
            }

        }
    }

    //Hàm CheckInputElements để ép người dùng nhập vào các phần tử trong mảng là số nguyên
    public static int CheckInputElements()
    {
        while (true)
        {
            string StringInput = Console.ReadLine();
            try
            {
                if (string.IsNullOrEmpty(StringInput))
                {
                    Console.Write("The number must not be empty!\nEnter an integer number again: ");
                }
                else
                {
                    int IntegerInput = int.Parse(StringInput);
                    return IntegerInput;

                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid number!");
                Console.Write("Enter an number: ");
            }

        }
    }
    public static void Main(string[] args)
    {
        Console.Write("Enter number of elements in array: ");
        int a = CheckInputPositive();
        int[] arr = new int[a];
        Console.WriteLine("Input elements into array:");

        //Vòng lặp For để người dùng nhập vào từng phần tử trong mảng
        for (int i = 0; i < a; i++)
        {
            Console.Write("arr["+(i+1)+"] = ");
            arr[i] = CheckInputElements();
        }
        int max = arr[0];
        int min = arr[0];

        //Vòng lặp For tìm giá trị lớn nhất và bé nhất trong mảng
        for(int i = 1; i < a; i++) 
        {
            if (arr[i] > max)
                max = arr[i];
            if (arr[i] < min)
                min = arr[i];
        }

        Console.Write("Array inputted is: [");
        //Vòng lặp For để in ra từng phần tử trong mảng
        for(int i=0; i < a; i++)
        {
            Console.Write(arr[i] + " ");
        }
        Console.WriteLine("]");
        Console.WriteLine("The maximum is: " + max);
        Console.WriteLine("The minimum is: " + min);
        Console.ReadLine();
    }
}