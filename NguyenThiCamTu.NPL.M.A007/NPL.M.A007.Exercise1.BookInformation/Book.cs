﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise1.BookInformation
{
    internal class Book
    {
        public string? BookName { get; set; }
        public int Isbn { get; set; }
        public string? AuthorName { get; set; }
        public string? PublisherName { get; set; }
        
        public Book() {
        }
        public Book(string? BookName, int Isbn, string? AuthorName, string? PublisherName)
        {
            this.BookName = BookName;
            this.Isbn = Isbn;
            this.AuthorName = AuthorName;
            this.PublisherName = PublisherName;
        }

        public string GetBookInformation()
        {
            string information = String.Format("{0,-15} {1,-40} {2,-30} {3,-30}\n",
                                               "ISBN Number", "Book Name", "Author Name", "Publisher Name");
                   information += String.Format("{0,-15} {1,-40} {2,-30} {3,-30}\n",
                                               this.Isbn, this.BookName, this.AuthorName, this.PublisherName);
            return information;
        }
    }
    
}
