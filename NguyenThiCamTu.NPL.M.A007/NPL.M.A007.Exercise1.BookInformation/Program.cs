﻿using NPL.M.A007.Exercise1.BookInformation;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;

Book book = new Book("Harry Potter",123, "J.K.Rowling", "Kim Dong");
Console.WriteLine(book.GetBookInformation());

Book book1 = new Book("Tôi thấy hoa vàng trên cỏ xanh", 124, "Nguyễn Nhật Ánh", "Nhà xuất bản Trẻ");
Console.WriteLine(book1.GetBookInformation());