﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2.CarShop
{
    internal class Sedan : Car
    {
        public int Length { get; set; }

        public Sedan() { }
        public Sedan(
            decimal Speed,
            double RegularPrice,
            string Color,
            int Length) : base (Speed, RegularPrice, Color) 
        {
            this.Length = Length;
        }
        public override double GetSalePrice()
        {
            if (this.Length > 20)
                return this.RegularPrice - this.RegularPrice * 0.05;
            else
                return this.RegularPrice - this.RegularPrice * 0.1;
        }
        public void DisplaySalePrice()
        {
            Console.WriteLine("The sale prices of Sedan Car :");
            string information = String.Format("{0, -10} {1, -20} {2, -12} {3, -13} {4, -13}\n",
                                               "Speed", "RegularPrice", "Color", "Length", "Sales Price");
            information += String.Format("{0, -10} {1, -20} {2, -12} {3, -13} {4, -13}\n",
                                         Speed, RegularPrice, Color, Length, GetSalePrice());
            Console.WriteLine(information);
        }
    }
}
