﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2.CarShop
{
    internal class MyOwnAutoShop
    {
        Sedan sedan = new Sedan(100, 10000, "Black", 150);
        List<Ford> listFord = new List<Ford>();
        Ford ford1 = new Ford(150, 10000, "White", 1990, 1000);
        Ford ford2 = new Ford(200, 10000, "Red", 2000, 2000);      
        Truck truck1 = new Truck(160, 10000, "Pink", 200);
        Truck truck2 = new Truck(145, 10000, "Blue", 200);
        
        public void DisplaySalePrice()
        {
           sedan.DisplaySalePrice();
           ford1.DisplaySalePrice();
           ford2.DisplaySalePrice();
           truck1.DisplaySalePrice();
           truck2.DisplaySalePrice();
        }
    }
}
