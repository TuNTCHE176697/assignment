﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2.CarShop
{
    internal class Truck : Car
    {
        public int Weight { get; set; }
        public Truck() { }
        public Truck( 
            int Speed,
            double RegularPrice,
            string Color,
            int Weight) :base(Speed, RegularPrice, Color) 
        {
            this.Weight = Weight;
        }
        public override double GetSalePrice()
        {
            if(this.Weight > 2000)
            {
                return this.RegularPrice - (this.Weight * 0.1);
            }
            else
            {
                return this.RegularPrice - (this.RegularPrice * 0.2);
            }
        }
        public void DisplaySalePrice()
        {
            Console.WriteLine("The sale prices of Truck Car :");
            string information = String.Format("{0, -10} {1, -20} {2, -12} {3, -13} {4, -13}\n",
                                               "Speed", "RegularPrice", "Color", "Weight", "Sales Price");
            information += String.Format("{0, -10} {1, -20} {2, -12} {3, -13} {4, -13}\n",
                                         Speed, RegularPrice, Color, Weight, GetSalePrice());
            Console.WriteLine(information);
        }

    }
}
