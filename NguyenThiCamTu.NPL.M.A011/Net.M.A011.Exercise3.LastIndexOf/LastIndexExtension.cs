﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A011.Exercise3.LastIndexOf
{
    internal static class LastIndexExtension
    {
        //Hàm LastIndexOf<T> trả về vị trí cuối cùng của elementValue trong mảng array
        public static int LastIndexOf<T>(this T[] array, T  elementValue)
        {
            int index = 0;
            for(int i = 0; i < array.Length; i++)
            {
                if (array[i].Equals(elementValue))
                {
                    index = i;
                }
            }
            return index;
        }    
    }
}
