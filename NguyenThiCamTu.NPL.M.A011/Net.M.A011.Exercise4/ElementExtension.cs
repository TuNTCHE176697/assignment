﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A011.Exercise4
{
    internal static class ElementExtension
    {
        //Hàm ElementOfOrder2 trả về phần tử lớn thứ 2 trong mảng số nguyên
        public static int ElementOfOrder2(this int[] array) 
        {
            if (array.Length < 2)
            {
                throw new InvalidOperationException("Array must have minimum 2 elements");
            }

            return array.OrderByDescending(x => x).ElementAt(1);
        }

        //Hàm ElementOfOrder<T> trả về phần tử lớn thứ order trong mảng array
        public static T ElementOfOrder<T>(this T[] array, int order) where T : IComparable<T>
        {
            if (order < 1 || order > array.Length)
            {
                throw new ArgumentException("Order is invalid!.");
            }

            return array.OrderByDescending(x => x).ElementAt(order - 1);
        }

        //Hàm RemoveDuplicate<T> xóa các phần tử trùng lặp trong mảng Array
        public static T[] RemoveDuplicate<T>(this T[] array)
        {
            List<T> listBefore = array.ToList();
            List<T> listAfter = new List<T>();
            foreach (T i in listBefore)
            {
                if (listAfter.Contains(i))
                { continue; }
                else
                { listAfter.Add(i); }
            }
            return listAfter.ToArray();
        }
    }
}
