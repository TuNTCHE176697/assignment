﻿
using Net.M.A011.Exercise4;

int[] numbers = new int[] { 3, 2, 5, 6, 1, 7, 7, 5, 2 };
numbers = numbers.RemoveDuplicate<int>();
Console.WriteLine("The 2nd largest number in the array using ElementOfOrder2(): "+numbers.ElementOfOrder2());
Console.WriteLine("The 2nd largest number in the array using ElementOfOrder(2): " + numbers.ElementOfOrder<int>(2));
Console.WriteLine("The 3rd largest number in the array using ElementOfOrder(3): " + numbers.ElementOfOrder<int>(3));
Console.WriteLine("The 20th largest number in the array using ElementOfOrder(20): " + numbers.ElementOfOrder<int>(20));


