﻿using Net.M.A011.Exercise2.RemoveDuplicate;

int[] array = new int[] { 1, 2, 3, 3, 5, 6, 5, 2 };
array = array.RemoveDuplicate();
Console.WriteLine("Array after removing duplicate integer elements using RemoveDuplicate()");
foreach (int i in array)
{
    Console.Write(i + " ");
}    
Console.WriteLine();

string[] name = new string[] { "Hung", "Vu", "Van", "Hung", "Quang", "Huy", "Vu" };
name = name.RemoveDuplicate<String>();
Console.WriteLine("Array after removing duplicate string elements using RemoveDuplicate<string>");
foreach (string s in name)
{
    Console.Write(s + " ");
}    

Console.ReadKey();