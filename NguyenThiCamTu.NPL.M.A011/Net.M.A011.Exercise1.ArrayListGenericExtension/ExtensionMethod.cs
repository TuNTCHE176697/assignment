﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A011.Exercise1.ArrayListGenericExtension
{
    internal static class ExtensionMethod
    {
        //Hàm CountInt đếm số lượng phần tử có kiểu dữ liệu int trong ArrayList.
        public static int CountInt(this ArrayList array)
        {
            //foreach (var element in array)
            //{
            //    if (element.GetType() == typeof(int))
            //{
            //        count++;
            //}

            return array.OfType<int>().Count();
        }

        // Hàm CountOf đếm số lượng phần tử có kiểu dữ liệu là dataType trong ArrayList.
        public static int CountOf(this ArrayList array, Type dataType)
        {
            //foreach (var element in array)
            //{
            //    if (element.GetType() == dataType)
            //{
            //        count++;
            //}

            return array.Cast<object>().Count(item => dataType.IsInstanceOfType(item));
        }

        // Hàm CountOf<T> đếm số lượng phần tử có kiểu dữ liệu là T trong ArrayList.
        public static int CountOf<T>(this ArrayList array)
        {
            //foreach (var element in array)
            //{
            //    if (element.GetType() == typeof(T))
            //{
            //        count++;
            //}
            return array.OfType<T>().Count();
        }

        // Phương thức mở rộng này trả về giá trị lớn nhất của kiểu T nếu T là kiểu số, ngược lại ném một ngoại lệ.
        public static T MaxOf<T>(this ArrayList array) where T : IComparable
        {
            //Type type = typeof(T);
            //if (type.IsPrimitive && type != typeof(bool) && type != typeof(char))
            //{
            //    T max = default(T);
            //    foreach (var element in array)
            //    {

            //        if (element.GetType()==typeof(T))
            //        {
            //            T temp = ((T)Convert.ChangeType(element, typeof(T)));
            //            if (temp.CompareTo(max) > 0)
            //            {
            //                max = temp;
            //            }
 
            //        }

            //    }
            //    return max;
            //}
            //else
            //{
            //    throw new Exception("Không tìm thấy phần tử có kiểu số cụ thể trong ArrayList.");
            //}

            var numericItems = array.OfType<T>();
            if (numericItems.Any())
            {
                return numericItems.Max();
            }
            throw new InvalidOperationException("Không tìm thấy phần tử có kiểu số cụ thể trong ArrayList.");
        }

    }
}
