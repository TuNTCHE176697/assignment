﻿using Net.M.A011.Exercise1.ArrayListGenericExtension;
using System.Collections;
ArrayList input = new ArrayList();
input.Add("Hieu");
input.Add("Hoang");
input.Add(1);
input.Add(2);
input.Add("Trong");
input.Add(3.9d);

Console.Write("Number of elements in the array has datatype is int using CountInt(): ");
Console.WriteLine(input.CountInt());

Console.Write("Number of elements in the array has datatype is int using CountOf(typeof(int)): ");
Console.WriteLine(input.CountOf(typeof(int)));

Console.Write("Number of elements in the array that has data type String using CountOf<T>:");
Console.WriteLine(input.CountOf<string>());

Console.Write("The max element of data type int in array using MaxOf<T>: ");
Console.Write(input.MaxOf<int>());

Console.ReadKey();
