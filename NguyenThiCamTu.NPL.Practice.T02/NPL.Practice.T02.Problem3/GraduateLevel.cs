﻿enum GraduateLevel
{
    Excellent,
    VeryGood,
    Good,
    Average,
    Failed
}