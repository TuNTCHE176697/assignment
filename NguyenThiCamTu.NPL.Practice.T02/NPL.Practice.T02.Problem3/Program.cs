﻿
using NPL.Practice.T02.Problem3;

Console.InputEncoding = System.Text.Encoding.Unicode;
Console.OutputEncoding = System.Text.Encoding.Unicode;

StudentManagement management = new StudentManagement();
while (true)
{
    Console.WriteLine("Student Management Program");
    Console.WriteLine("1. Add a new Student");
    Console.WriteLine("2. Get Student Certificates");
    Console.WriteLine("0. Exit");

    Console.Write("Enter menu option: ");
    int choice = Validate.ValidateChoice(0, 9);

    switch (choice)
    {
        case 1:
            management.AddStudent();
            break;

        case 2:
            management.ShowCertificates();
            break;

        case 0:
            // Thoát khỏi ứng dụng
            Environment.Exit(0);
            break;

        default:
            Console.WriteLine("Invalid option!");
            break;
    }
}
