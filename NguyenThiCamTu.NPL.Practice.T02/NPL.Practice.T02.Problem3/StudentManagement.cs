﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem3
{
    internal class StudentManagement
    {
        List<Student> students = new List<Student>();
        
        public void AddStudent()
        {
            Console.WriteLine("Enter Student Information: ");
            Student student = new Student();
            Console.Write("Enter Student ID: ");
            int id = 0;
            bool a = true;
            while(a)
            {
                id = Validate.ValidateNumber("Student ID");
                int count = (from st in students
                            where st.Id == id
                            select st).Count();
                if (count > 0)
                {
                    Console.WriteLine("Duplicate Student ID! Enter Student ID again!");
                    Console.Write("Enter Student ID: ");
                }
                else
                    a=false;
            } 
            student.Id = id;

            Console.Write("Enter Student Name: ");
            student.Name = Validate.ValidateName();

            Console.Write("Enter Student Start Date: ");
            student.StartDate = Validate.ValidateStartDate();

            Console.Write("Enter SQL Mark: ");
            student.SqlMark = Validate.ValidateMark(0,10,"SQL Mark");

            Console.Write("Enter Csharp Mark: ");
            student.CsharpMark = Validate.ValidateMark(0, 10, "Csharp Mark");

            Console.Write("Enter Dsa Mark: ");
            student.DsaMark = Validate.ValidateMark(0, 10, "Dsa Mark");

            student.Graduate();

            students.Add(student);

            Console.Write("Do you want to continue? (Yes/No): ");
            string choice = Validate.ValidateContinueChoice("Yes", "No");
            if(choice.ToLower().Equals("yes"))
            {
                AddStudent();
            }    
        }

        public void ShowCertificates()
        {
            Console.WriteLine("List Of Certificates: ");
            foreach(Student student in students)
            {
                Console.WriteLine(student.GetCertificate());
            }
        }
    }
}
