﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem3
{
    internal class Student : IGraduate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public decimal SqlMark { get; set; }
        public decimal CsharpMark { get; set; }
        public decimal DsaMark { get; set; }
        public decimal GPA { get; set; }
        public GraduateLevel Level { get; set; }

        public Student()
        {
        }

        public Student(int id, string name, DateTime startDate, decimal sqlMark, decimal csharpMark, decimal dsaMark)
        {
            this.Id = id;
            this.Name = name;
            this.StartDate = startDate;
            this.SqlMark = sqlMark;
            this.CsharpMark = csharpMark;
            this.DsaMark = dsaMark;
         
        }

        public void Graduate()
        {
            GPA = (SqlMark + CsharpMark + DsaMark) / 3;
            if (GPA >= 9)
            {
                Level = GraduateLevel.Excellent;
            }
            else if (GPA >= 8 && GPA < 9)
            {
                Level = GraduateLevel.VeryGood;
            }
            else if (GPA >= 7 && GPA < 8)
            {
                Level = GraduateLevel.Good;
            }
            else if (GPA >= 5 && GPA < 7)
                Level = GraduateLevel.Average;
            else
                Level = GraduateLevel.Failed;

        }

        public string GetCertificate()
        {
            return $"Name: {Name}, SqlMark: {SqlMark}, CsharpMark: {CsharpMark}, DasMark: {DsaMark}, GPA: {Math.Round(GPA,2)}, Level: {Level}";
        }

    }
}

