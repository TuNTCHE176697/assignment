﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.Write("Enter size of integer array: ");
        int size = ValidateSize("Size of Integer Array");
        int[] array = new int[size];
        Console.WriteLine("Enter each integer element of array: ");
        for (int i = 0; i < array.Length; i++)
        {
            Console.Write("Element ["+(i+1)+"] = ");
            array[i] = ValidateNumber("Integer Element");
        }
        Console.Write("Enter Subarray Length: ");
        int length = ValidateLength("Subarray Length", array);
        int maxSum = FindMaxSubArray(array, length);

        Console.Write("Inputted Integer Array: [");
        
        for(int i = 0; i < array.Length; i++)
        {
            if(i == array.Length - 1)
            {
                Console.WriteLine(array[i]+"]");
            }
            else
            Console.Write(array[i]+ ", ");
        }
        Console.WriteLine("The Maximum contiguous Subarray Sum:" + maxSum);
        Console.ReadKey();

    }

    //Hàm FindMaxSubArray trả về tổng chuỗi con lớn nhất
    static public int FindMaxSubArray(int[] inputArray, int subLength)
    {
        int maxSum = int.MinValue;

        for (int i = 0; i <= inputArray.Length - subLength; i++)
        {
            int subarraySum = 0;
            for (int j = i; j < i + subLength; j++)
            {
                subarraySum += inputArray[j];
            }
            maxSum = Math.Max(maxSum, subarraySum);
        }
        return maxSum;
    }

    //Hàm ValidateLength dùng để ép người dùng nhập vào số nguyên lớn hơn 0 và bé hơn hoặc bằng kích thước mảng
    public static int ValidateLength(string character, int[] array)
    {
        while (true)
        {
            string StringInput = Console.ReadLine();
            try
            {
                if (string.IsNullOrEmpty(StringInput))
                {
                    Console.Write("The " + character + " must not be empty!\nEnter " + character + " again: ");
                }
                else
                {
                    int IntInput = int.Parse(StringInput);
                    if (IntInput <= 0)
                    {
                        Console.WriteLine("Invalid number! The " + character + " must be greater than 0!");
                        Console.Write("Enter " + character + " again: ");
                    }
                    else if(IntInput > array.Length)
                    {
                        Console.WriteLine("Invalid number! The " + character + " must be smaller or equal than input array length!");
                        Console.Write("Enter " + character + " again: ");
                    }    
                    else return IntInput;
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid number! The " + character + " must be greater than 0!");
                Console.Write("Enter " + character + " again: ");
            }
        }
    }

    //Hàm ValidateNumber dùng để ép người dùng nhập vào số nguyên 
    public static int ValidateNumber(string character)
    {
        while (true)
        {
            string StringInput = Console.ReadLine();
            try
            {
                if (string.IsNullOrEmpty(StringInput))
                {
                    Console.Write("The " + character + " must not be empty!\nEnter " + character + " again: ");
                }
                else
                {
                    int IntInput = int.Parse(StringInput);
                    return IntInput;
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid number! The " + character + " must be an integer!");
                Console.Write("Enter " + character + " again: ");
            }
        }
    }

    //Hàm ValidateNumber dùng để ép người dùng nhập vào số nguyên lớn hơn 0
    public static int ValidateSize(string character)
    {
        while (true)
        {
            string StringInput = Console.ReadLine();
            try
            {
                if (string.IsNullOrEmpty(StringInput))
                {
                    Console.Write("The " + character + " must not be empty!\nEnter " + character + " again: ");
                }
                else
                {
                    int IntInput = int.Parse(StringInput);
                    if (IntInput <= 0)
                    {
                        Console.WriteLine("Invalid number! The " + character + " must be greater than 0!");
                        Console.Write("Enter " + character + " again: ");
                    }
                    
                    else return IntInput;
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid number! The " + character + " must be an integer!");
                Console.Write("Enter " + character + " again: ");
            }
        }
    }

}
