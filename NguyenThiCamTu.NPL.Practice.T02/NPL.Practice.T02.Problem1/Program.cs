﻿
using System.ComponentModel.DataAnnotations;
internal class Program
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Enter Content Of Article: ");
        string content = ValidateContent("Content");
        Console.Write("Enter Max Length of Summary: ");
        int length = ValidateNumber("Max Length of Summary");
        string summary = GetArticleSummary(content, length);
        Console.WriteLine("Summary of Article:");
        Console.WriteLine(summary);
        Console.ReadKey();

    }
    public static string ValidateContent(string character)
    {
        while (true)
        {
            string StringInput = Console.ReadLine();
            if (string.IsNullOrEmpty(StringInput))
            {
                Console.Write("The " + character + " must not be empty!\nEnter " + character + " again: ");
            }
            else
            {
                return StringInput;
            }
        }
    }

    //Hàm ValidateNumber dùng để ép người dùng nhập vào số nguyên lớn hơn 0
    public static int ValidateNumber(string character)
    {
        while (true)
        {
            string StringInput = Console.ReadLine();
            try
            {
                if (string.IsNullOrEmpty(StringInput))
                {
                    Console.Write("The " + character + " must not be empty!\nEnter " + character + " again: ");
                }
                else
                {
                    int IntInput = int.Parse(StringInput);
                    if (IntInput <= 0)
                    {
                        Console.WriteLine("Invalid number! The " + character + " must be greater than 0!");
                        Console.Write("Enter " + character + " again: ");
                    }
                    else return IntInput;
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid number! The " + character + " must be greater than 0!");
                Console.Write("Enter " + character + " again: ");
            }
        }
    }

    //Hàm GetArticleSummary để trả về đoạn summary với content có nội dung tối da maxLength chữ
    static public string GetArticleSummary(string content, int maxLength)
    {
        //Loại bỏ khoảng trắng 2 bên nội dung Content
        content = content.Trim();

        //Tính chiều dài length của content
        int length = content.Length;

        //Nếu chiều dài length <= maxLength thì trả về toàn bộ nội dung content
        if (length <= maxLength)
        {
            return content;
        }
        else
        {
            //Tìm vị trí khoảng trắng cuối cùng trong chuỗi từ vị trí ban đầu đến vị trí maxlength
            int cutIndex = content.Substring(0, maxLength - 1).LastIndexOf(" ");
            //Nối phần nội dung content từ vị trí đầu tiên đến vị trí cutIndex và dấu ...
            content = content.Substring(0, cutIndex) + "...";
            return content;
        }
    }
}
